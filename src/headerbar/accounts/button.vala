/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Button showing current active account. */
[GtkTemplate (ui = "/org/adishatz/Mail/headerbar-accounts-button.ui")]
public class Mail.HeaderBar.Accounts.Button : Adw.Bin {

    /**
     * Emit the active_account_changed signal.
     *
     * A new account has been selected in button's popover
     */
    public signal void active_account_changed();

    /** Active account. */
    public Engine.Accounts.Account? active {
        get; private set; default = null;
    }

    [GtkChild] private unowned Adw.Avatar avatar;
    [GtkChild] private unowned Popover popover;

    /** Init button and popover. */
    public void init() {
        var accounts = Engine.Accounts.Manager.get_default();
        // Be sure engine has finished handling new account
        accounts.account_added.connect_after(on_account_added);
        accounts.account_removed.connect(on_account_removed);
        this.popover.init();
    }

    /**
     * Add account to popover.
     *
     * @param account - {@link Engine.Accounts.Account}
     */
    private void on_account_added(Engine.Accounts.Account account) {
        this.popover.add_account(account);
    }

    /**
     * Remove account from popover.
     *
     * @param account - {@link Engine.Accounts.Account}
     */
    private void on_account_removed(Engine.Accounts.Account account) {
        this.popover.remove_account(account);
    }

    /** Save active account to settings and emit active_account_changed(). */
    [GtkCallback]
    private void on_account_activated(PopoverRow row) {
        if (row.id == null) {
            this.active = null;
            this.avatar.text = "";
            this.avatar.icon_name = "system-users-symbolic";
        } else {
            var accounts = Engine.Accounts.Manager.get_default();
            this.active = accounts.get_account_for_id(row.id);
            this.avatar.text = row.avatar;
        }

        var settings = Settings.Store.get_default();
        settings.startup_account_id = row.id == null ? "" : row.id;
        active_account_changed();
    }

    /** Hide button if no more account available. */
    [GtkCallback]
    private void on_account_selected(PopoverRow? row) {
        if (row == null) {
            this.hide();
        } else {
            this.show();
        }
    }
}