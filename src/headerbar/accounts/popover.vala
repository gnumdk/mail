/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Popover showing account list. */
[GtkTemplate (ui = "/org/adishatz/Mail/headerbar-accounts-popover.ui")]
public class Mail.HeaderBar.Accounts.Popover : Gtk.Popover {

    /**
     * Emit the account_activated signal.
     *
     * @param row - {@link PopoverRow}
     */
    public signal void account_activated(PopoverRow row);

    /**
     * Emit the account_selected signal.
     *
     * @param row - {@link PopoverRow}
     */
    public signal void account_selected(PopoverRow row);

    [GtkChild] private unowned Gtk.ListBox listbox;

    private uint timeout_id = 0;

    construct {
        // Activate first account if none has been activated
        // after a small delay.
        this.timeout_id = GLib.Timeout.add(1000, () => {
            var row = this.listbox.get_first_child();
            row.activate();
            this.timeout_id = 0;
            return Source.REMOVE;
        });
    }

    /** Init popover adding default row. */
    public void init() {
        var row = new PopoverRow(
            null,
            _("Show all emails"),
            _("All accounts")
        );
        this.listbox.append(row);
        restore_account_selection(row);
    }

    /**
     * Add an account to popover.
     *
     * @param account - {@link Engine.Accounts.Account}
     */
    public void add_account(Engine.Accounts.Account account) {
        var row = new PopoverRow(
            account.id,
            account.name,
            account.provider
        );
        this.listbox.append(row);
        restore_account_selection(row);
    }

    /**
     * Remove an account from popover.
     *
     * @param account - {@link Engine.Accounts.Account}
     */
    public void remove_account(Engine.Accounts.Account account) {
        var row = this.listbox.get_first_child();
        while (row != null) {
            if (((PopoverRow) row).id == account.id) {
                this.listbox.remove(row);
                break;
            }
            row = this.listbox.get_next_sibling();
        }
    }

    /*
     * Restore account selection for row.
     *
     * If row was selected on previous run, restore it as selected
     */
    private void restore_account_selection(PopoverRow row) {
        var settings = Settings.Store.get_default();
        string? startup_account_id = settings.startup_account_id == "" ?
            null : settings.startup_account_id;
        if (row.id == startup_account_id) {
            row.activate();
            if (this.timeout_id > 0) {
                GLib.Source.remove(this.timeout_id);
                this.timeout_id = 0;
            }
        }
    }

    /* Hide popover when row is activated. */
    [GtkCallback]
    private void on_row_activated(Gtk.ListBox listbox, Gtk.ListBoxRow row) {
        account_activated((PopoverRow) row);
        this.popdown();
    }

    /* Just notify for row being selected. */
    [GtkCallback]
    private void on_row_selected(Gtk.ListBox listbox, Gtk.ListBoxRow? row) {
        account_selected((PopoverRow) row);
    }

}