/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** An account row. */
[GtkTemplate (ui = "/org/adishatz/Mail/headerbar-accounts-popover-row.ui")]
public class Mail.HeaderBar.Accounts.PopoverRow : Gtk.ListBoxRow {

    [GtkChild] private unowned Adw.Avatar avatar_image;
    [GtkChild] private new unowned Gtk.Label name_label;
    [GtkChild] private unowned Gtk.Label provider_label;

    /** Row id. */
    public string? id { get; construct; }

    /** Row name. */
    public new string name { get { return this.name_label.label; } }

    /** Row provider. */
    public string provider { get { return this.provider_label.label; } }

    /** Row avatar. */
    public string avatar { get { return this.avatar_image.text; } }

    /**
     * Create a new row.
     *
     * @param id - Row id
     * @param name - Row name
     * @param provider - Row provider
     */
    public PopoverRow(string? id, string name, string provider) {
        Object( id: id );
        if (id == null) {
            this.avatar_image.icon_name = "system-users-symbolic";
        } else {
            this.avatar_image.text = name.replace("@", " ");
        }
        this.name_label.label = name;
        this.provider_label.label = provider;
    }
}