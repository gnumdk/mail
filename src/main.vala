/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

public extern const string GETTEXT_PACKAGE;
public extern const string _APP_ID;
public extern const string _LOCALE_DIR;


int main (string[] args) {
    Intl.bindtextdomain(GETTEXT_PACKAGE, _LOCALE_DIR);
    Intl.bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
    Intl.textdomain(GETTEXT_PACKAGE);

    var app = new Mail.Application ();
    return app.run (args);
}
