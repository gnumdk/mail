/*
 * Copyright 2016 Software Freedom Conservancy Inc
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

namespace Mail.Utils.Hash {

    /** To be used as hash_func for Gee collections. */
    public inline uint uint64_hash_func(uint64? n) {
        return hash_memory((uint8 *) n, sizeof(uint64));
    }

    /** To be used as equal_func for Gee collections. */
    public bool uint64_equal_func(uint64? a, uint64? b) {
        uint64 *bia = (uint64 *) a;
        uint64 *bib = (uint64 *) b;

        return (*bia) == (*bib);
    }

    /**
     * A rotating-XOR hash that can be used to hash memory buffers of any size
     */
    public uint hash_memory(void *ptr, size_t bytes) {
        if (ptr == null || bytes == 0) {
            return 0;
        }

        uint8 *u8 = (uint8 *) ptr;

        // initialize hash to first byte value and then rotate-XOR from there
        uint hash = *u8;
        for (int ctr = 1; ctr < bytes; ctr++) {
            hash = (hash << 4) ^ (hash >> 28) ^ (*u8++);
        }

        return hash;
    }
}
