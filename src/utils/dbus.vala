/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

namespace Mail.Utils.DBus {

    const string SETTINGS_DBUS_NAME = "org.gnome.Settings";
    const string SETTINGS_DBUS_PATH = "/org/gnome/Settings";
    const string SETTINGS_DBUS_INTERFACE = "org.gtk.Actions";

    /** Launch settings manager on GNOME Online accounts panel */
    async void launch_online_accounts() {
        try {
            // This method was based on the implementation from:
            // https://gitlab.gnome.org/GNOME/gnome-calendar/blob/master/src/gcal-source-dialog.c,
            // Courtesy Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
            GLib.DBusProxy settings = yield new GLib.DBusProxy.for_bus(
                GLib.BusType.SESSION,
                GLib.DBusProxyFlags.NONE,
                null,
                SETTINGS_DBUS_NAME,
                SETTINGS_DBUS_PATH,
                SETTINGS_DBUS_INTERFACE,
                null
            );

            // @s "launch-panel"
            // @av [<@(sav) ("online-accounts", [<@s "add">, <@s "google">])>]
            // @a{sv} {}

            GLib.Variant[] args =  {};

            GLib.Variant command = new GLib.Variant.tuple(
                new GLib.Variant[] {
                    new GLib.Variant.string("online-accounts"),
                    new GLib.Variant.array(
                        GLib.VariantType.VARIANT, args
                    )
                }
            );

            GLib.Variant params = new GLib.Variant.tuple(
                new GLib.Variant[] {
                    new GLib.Variant.string("launch-panel"),
                    new GLib.Variant.array(
                        GLib.VariantType.VARIANT,
                        new GLib.Variant[] {
                            new GLib.Variant.variant(command)
                        }
                    ),
                    new GLib.Variant("a{sv}")
                }
            );

            yield settings.call(
                "Activate", params, GLib.DBusCallFlags.NONE, -1, null
            );
        } catch (GLib.Error error) {
            warning("Can't launch settings: %s", error.message);
        }
    }
}