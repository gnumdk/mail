/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Reusable cancellable. */
public class Mail.Utils.Cancellable {

    public GLib.Cancellable cancellable {
        get;
        private set;
        default = new GLib.Cancellable();
    }

    /** Get a new Cancellable. */
    public Cancellable() {
    }

    /** Cancel current cancellable and prepare a new one. */
    public void cancel() {
        this.cancellable.cancel();
        this.cancellable = new GLib.Cancellable();
    }
}
