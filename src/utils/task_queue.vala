/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

public interface Mail.Utils.TaskQueueRunner : GLib.Object {
    public abstract async bool run_queue();
}

/** Running a {@link TaskQueueRunner} if not empty */
public class Mail.Utils.TaskQueue {

    private weak TaskQueueRunner runner;

    private bool started = false;

    /** Create a new TaskQueue. */
    public TaskQueue(TaskQueueRunner runner) {
        this.runner = runner;
    }

    /**
     * Start runner
     * Will do nothing if running, force start if pending.
     *
     */
    public async void start() {
        if (started) {
            return;
        }

        this.started = true;
        while (yield this.runner.run_queue()) {}
        this.started = false;
    }
}
