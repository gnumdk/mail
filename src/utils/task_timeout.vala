/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

public interface Mail.Utils.TaskTimeoutRunner : GLib.Object {
    public abstract async void run_timeout();
}

/** Running a {@link TaskTimeoutRunner} at regular interval */
public class Mail.Utils.TaskTimeout {

    private uint interval;
    private weak TaskTimeoutRunner runner;

    private bool started = false;
    private uint timeout_id = 0;

    /** Create a new TaskTimeout. */
    public TaskTimeout(uint interval, TaskTimeoutRunner runner) {
        this.interval = interval;
        this.runner = runner;
        this.start.begin();
    }

    /**
     * Start runner
     * Will do nothing if running, force start if pending.
     *
     */
    public async void start() {
        // Already started
        if (this.started) {
            return;
        // We cancel any pending task
        } else if (timeout_id > 0) {
            GLib.Source.remove(this.timeout_id);
            this.timeout_id = 0;
        }

        this.started = true;
        yield this.runner.run_timeout();
        this.started = false;

        this.timeout_id = (int) GLib.Timeout.add_seconds(
            this.interval, () => {
                this.timeout_id = 0;
                start.begin();
                return false;
        });
    }
}