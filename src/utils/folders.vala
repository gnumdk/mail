/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

namespace Mail.Utils.Folders {

    /**
     * Get folder information.
     *
     * @param store - {@link Camel.Store}
     * @param path - Folder path
     * @param flags - {@Camel.StoreGetFolderInfoFlags} to control behavior
     * @param cancellable - {@link GLib.Cancellable}
     */
    async Camel.FolderInfo? get_folder_info(
                          Camel.Store store, string? path,
                          Camel.StoreGetFolderInfoFlags flags,
                          GLib.Cancellable cancellable) {
        try {
            return yield store.get_folder_info(
                path,
                flags,
                GLib.Priority.DEFAULT,
                cancellable
            );
        } catch (GLib.Error error) {
            warning(error.message);
        }
        return null;
    }

    /**
     * Get folder for account and information.
     *
     * @param account - {@link Engine.Accounts.Account}
     * @param folder_info - {@link Camel.FolderInfo}
     * @param cancellable - {@link GLib.Cancellable}
     */
    async Engine.Folders.Folder? get_folder(
                                   Engine.Accounts.Account account,
                                   Camel.FolderInfo folder_info,
                                   GLib.Cancellable cancellable) {

        try {
            return (Engine.Folders.Folder) yield new Models.FolderList.Folder(
                folder_info,
                account,
                cancellable
            );
        } catch {
            // Invalid folder (virtual for example)
        }
        return null;
    }

    /**
     * Get camel folder.
     *
     * @param store - {@link Camel.Store}
     * @param path - Folder path
     * @param cancellable - {@link GLib.Cancellable}
     */
    async Camel.Folder? get_camel_folder(Camel.Store store,
                                         string path,
                                         GLib.Cancellable cancellable) {

        try {
            return yield store.get_folder(
                path,
                Camel.StoreGetFolderFlags.NONE,
                GLib.Priority.DEFAULT,
                cancellable
            );
        } catch (GLib.Error error) {
            warning(error.message);
        }
        return null;
    }

    /**
     * Get mail account extension for service.
     *
     * @param service - {@link Camel.Service}
     *
     * @return extension
     */
    E.SourceMailAccount? get_mail_account_extension(Camel.Service service) {
        var session = Engine.Session.get_default();
        E.Source source = session.ref_source(service.uid);

        if (source != null &&
            source.has_extension(E.SOURCE_EXTENSION_MAIL_ACCOUNT)) {
            return (E.SourceMailAccount) source.get_extension(
                E.SOURCE_EXTENSION_MAIL_ACCOUNT
            );
        }
        return null;
    }

    /**
     * Get mail extension for mail account extension.
     *
     * @param extension - {@link E.SourceMailAccount}
     * @param extension_string - {@link Camel.Service}
     *
     * @return extension
     */
    E.SourceExtension? get_mail_extension(E.SourceMailAccount extension,
                                          string extension_string) {

        string? ident = extension.dup_identity_uid();
        if (ident == null)
            return null;

        var session = Engine.Session.get_default();
        E.Source source = session.ref_source(ident);
        if (source == null)
            return null;

        return source.get_extension(extension_string);
    }

    /**
     * Get folder name for archives.
     *
     * @param account - {@link Engine.Accounts.Account}
     *
     * @return name
     */
    string get_name_for_archives(Engine.Accounts.Account account) {
        var account_ext = get_mail_account_extension(account.store);
        if (account_ext == null)
            return "";

        string? uri = account_ext.dup_archive_folder();
        if (uri != null) {
            string[] split = uri.split("/");
            if (split.length > 1)
                return split[split.length - 1];
        }
        return "";
    }

    /**
     * Get folder name for sent.
     *
     * @param account - {@link Engine.Accounts.Account}
     *
     * @return name
     */
    string get_name_for_sent(Engine.Accounts.Account account) {
        var account_ext = get_mail_account_extension(account.store);
            if (account_ext == null)
                return "";

            var extension = get_mail_extension(
                account_ext, E.SOURCE_EXTENSION_MAIL_SUBMISSION
            );
            if (extension != null) {
                string? uri = (
                    (E.SourceMailSubmission) extension
                ).dup_sent_folder();
                if (uri != null) {
                    string[] split = uri.split("/");
                    if (split.length > 1)
                        return split[split.length - 1];
                }
            }
            return "";
    }

    /**
     * Get folder name for drafts.
     *
     * @param account - {@link Engine.Accounts.Account}
     *
     * @return name
     */
    string get_name_for_drafts(Engine.Accounts.Account account) {
        var account_ext = get_mail_account_extension(account.store);
            if (account_ext == null)
                return "";

            var extension = get_mail_extension(
                account_ext, E.SOURCE_EXTENSION_MAIL_COMPOSITION
            );
            if (extension != null) {
                string? uri = (
                    (E.SourceMailComposition) extension
                ).dup_drafts_folder();
                if (uri != null) {
                    string[] split = uri.split("/");
                    if (split.length > 1)
                        return split[split.length - 1];
                }
            }
            return "";
    }

    /**
     * Get folder name for templates.
     *
     * @param account - {@link Engine.Accounts.Account}
     *
     * @return name
     */
    string get_name_for_templates(Engine.Accounts.Account account) {
        var account_ext = get_mail_account_extension(account.store);
            if (account_ext == null)
                return "";

            var extension = get_mail_extension(
                account_ext, E.SOURCE_EXTENSION_MAIL_COMPOSITION
            );
            if (extension != null) {
                string? uri = (
                    (E.SourceMailComposition) extension
                ).dup_templates_folder();
                if (uri != null) {
                    string[] split = uri.split("/");
                    if (split.length > 1)
                        return split[split.length - 1];
                }
            }
            return "";
    }
}