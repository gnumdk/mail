/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

namespace Mail.Utils.ListStore {

    /**
     * Get folder position in model.
     *
     * @param model - {@link GLib.ListModel}
     * @param path - folder path or null
     */
    public uint? get_position(GLib.ListModel model, string? path) {
        for (var i=0; i < model.get_n_items(); i++) {
            var folder = (Engine.Folders.Folder) model.get_object(i);
            if (folder.path == path) {
                return i;
            }
        }
        return null;
    }

    /**
     * Get model folders.
     *
     * @param model - {@link GLib.ListModel}
     *
     * @return a map {folder path : folder}
     */
    public Gee.HashMap<string, Engine.Folders.Folder> get_folders(
                                                        GLib.ListModel model) {
        var folders = new Gee.HashMap<string, Engine.Folders.Folder>();

        for (var i=0; i < model.get_n_items(); i++) {
            var folder = (Engine.Folders.Folder) model.get_item(i);
            folders.set(folder.path, folder);
        }

        return folders;
    }
}