/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 * Copyright © 2016 Software Freedom Conservancy Inc.
 * Copyright © 2020-2021 Michael Gratton <mike@vee.net>
 */

namespace Mail.Utils.Mail {

    /**
     * Get senders for messages.
     *
     * @param messages - {@link Gee.Collection<Engine.Message>}
     *
     * @return a string with names
     */
    string get_senders_for_messages(
                        Gee.Collection<Engine.Conversations.Message> messages) {
        var internet_address = new Camel.InternetAddress ();
        GLib.GenericArray<string> names = new GLib.GenericArray<string>();
        int index = 0;

        foreach (var message in messages) {
            if (internet_address.decode(message.info.from) > 0) {
                unowned string? user_name;
                unowned string? user_address;
                internet_address.get (index, out user_name, out user_address);
                if (user_name != null && user_name != "") {
                    if (!names.find_with_equal_func(user_name, (a, b) => {
                        return a == b;
                    })) {
                        names.add(user_name);
                    }
                } else if (user_address != null && user_address != "") {
                    if (!names.find_with_equal_func(user_address, (a, b) => {
                        return a == b;
                    })) {
                        names.add(user_address);
                    }
                }
                index ++;
            }
        }
        return string.joinv(", ", names.data);
    }

    /**
     * Returns the Subject: line stripped of reply and forwarding prefixes.
     *
     * Strips ''all'' prefixes, meaning "Re: Fwd: Soup's on!" will return "Soup's on!"
     *
     * Returns an empty string if the Subject: line is empty (or is empty after stripping prefixes).
     */
    public string strip_prefixes(string? subject) {
        if (subject == null) {
            return "";
        }

        string subject_base = subject;
        bool changed = false;
        do {
            string stripped;
            try {
                Regex re_regex = new Regex("^(?i:Re:\\s*)+");
                stripped = re_regex.replace(subject_base, -1, 0, "");

                Regex fwd_regex = new Regex("^(?i:Fwd:\\s*)+");
                stripped = fwd_regex.replace(stripped, -1, 0, "");
            } catch (RegexError e) {
                debug("Failed to clean up subject line \"%s\": %s", subject, e.message);
                break;
            }

            changed = (stripped != subject_base);
            if (changed)
                subject_base = stripped;
        } while (changed);

        return subject_base;
    }
}