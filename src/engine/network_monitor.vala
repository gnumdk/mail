/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Advanced network monitor. */
public class Mail.Engine.NetworkMonitor {

    public signal void network_available(bool online);

    private static uint START_DELAY=1500;

    private static NetworkMonitor _network_monitor;
    public static unowned NetworkMonitor get_default() {
        if (_network_monitor == null) {
            _network_monitor = new NetworkMonitor();
        }
        return _network_monitor;
    }

    private GLib.NetworkMonitor monitor = GLib.NetworkMonitor.get_default();

    private uint network_timeout_id = 0;

    private GLib.GenericArray<unowned Accounts.Account> accounts =
        new GLib.GenericArray<unowned Accounts.Account>();

    /** Create a new network monitor. */
    public NetworkMonitor() {
        // See Session constructor
        GLib.Timeout.add(START_DELAY, () => {
            notify_network_available(this.monitor.get_network_available());
            this.monitor.network_changed.connect(on_network_changed);
            return false;
        });
    }

    /**
     * Add account to monitoring.
     *
     * @param account - {@link Accounts.Account}
     */
    public void add_account(Accounts.Account account) {
        this.accounts.add(account);
        set_account_online_status.begin(
            account, this.monitor.get_network_available()
        );
    }

    /**
     * Remove account from monitoring.
     *
     * @param account - {@link Accounts.Account}
     */
    public void remove_account(Accounts.Account account) {
        this.accounts.remove_fast(account);
    }

    /* Set account online status. */
    private async void set_account_online_status(Accounts.Account account,
                                                 bool online) {
        if (!Session.get_default().online || account.store.online == online)
            return;

        if (yield check_account_reachable(account)) {
            yield account.set_online(online);
        }
    }

    /*
     * Check account is reachable, this does not really test connection
     * to server.
     */
    private async bool check_account_reachable(Accounts.Account account) {
        var remote = new GLib.NetworkAddress(
            account.object.mail.imap_host,
            993 // We don't care
        );
        try {
            var cancellable = Controller.get_cancellable().cancellable;
            return yield this.monitor.can_reach_async(
                remote, cancellable
            );
        } catch {
            return false;
        }
    }

    /* Notify network availability changes. */
    private void notify_network_available(bool online) {
        Session.get_default().set_online(online);
        foreach (var account in this.accounts) {
            set_account_online_status.begin(account, online);
        }
        network_available(online);
    }

    /* Update network status if changed. */
    private void on_network_changed() {
        if (this.network_timeout_id > 0) {
            GLib.Source.remove(this.network_timeout_id);
        }
        this.network_timeout_id = GLib.Timeout.add(
            500,
            () => {
            bool online = this.monitor.get_network_available();
            this.network_timeout_id = 0;
            if (online != Session.get_default().get_online()) {
                notify_network_available(online);
            }
            return false;
        });
    }
}