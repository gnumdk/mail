/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Allows to sync messages and folders with remote servers. */
public class Mail.Engine.RemoteSync : Utils.TaskTimeoutRunner,
                                      Utils.TaskQueueRunner,
                                      GLib.Object {

    /** Folder store */
    public Camel.OfflineStore store {
        get; private set;
    }

    /** True is store is online. */
    public bool online { get; protected set; }

    private GLib.List<Folders.Folder> folders =
        new GLib.List<Folders.Folder>();

    private GLib.Queue<unowned Conversations.Message> queue =
        new GLib.Queue<unowned Conversations.Message>();

    private Utils.Cancellable cancellable = new Utils.Cancellable();

    private Utils.TaskTimeout task_timeout;
    private Utils.TaskQueue task_queue;

    /**
     * Create a new synchronizer for store.
     *
     * @param store - {@link Camel.OfflineStore} we want to synchronize
     */
    public RemoteSync(Camel.OfflineStore store) {
        this.store = store;
        this.task_timeout = new Utils.TaskTimeout(
            15 * 60,
            this
        );
        this.task_queue = new Utils.TaskQueue(this);
        this.notify["online"].connect(() => {
            this.cancellable.cancel();
            if (this.online) {
                sync_store.begin();
                this.task_timeout.start.begin();
                this.task_queue.start.begin();
            }
        });
    }

    /**
     * Register folder for sync.
     *
     * @param folder - {@link Folders.Folder} to register
     */
    public void register_folder(Folders.Folder folder) {
        debug("Registering updates on: %s\n", folder.path);
        lock (this.folders) {
            this.folders.append(folder);
            this.task_timeout.start.begin();
        }
    }

    /**
     * Unregister folder from sync.
     *
     * @param folder - {@link Folders.Folder} to unregister
     */
    public void unregister_folder(Folders.Folder folder) {
        debug("Unregistering updates on: %s\n", folder.path);
        lock (this.folders) {
            this.folders.remove(folder);
        }
    }

    /**
     * Check if sync has folder.
     *
     * @param path - Folder IMAP path
     *
     * @return True if folder present
     */
    public bool has_folder(string path) {
        foreach (var folder in this.folders) {
            if (folder.path == path) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add a message to sync queue.
     *
     * @param message - {@link Conversations.Message}
     */
    public void add_message_to_sync_queue(Conversations.Message message) {
        lock (this.queue) {
            this.queue.push_tail(message);
        }
        this.task_queue.start.begin();
    }

    public void remove_message_from_sync_queue(Conversations.Message message) {
        lock(this.queue) {
            this.queue.remove(message);
        }
    }

    /** Sync folders unless network is down */
    public async void run_timeout() {
        if (this.online) {
            try {
                unowned var folder = this.folders.first();
                while (folder != null) {
                    yield folder.data.refresh(this.cancellable.cancellable);
                    folder = folder.next;
                }
            } catch (GLib.Error error) {
                warning(error.message);
            }
        }
    }

    /* Sync messages unless queue is empty or network is down */
    public async bool run_queue() {
        if (this.online) {
            Conversations.Message message;
            uint length;

            lock (this.queue) {
                length = this.queue.length;
                if (length > 0) {
                    message = this.queue.pop_head();
                    yield message.sync(this.cancellable.cancellable);
                }
                length--;
            }

            return this.queue.length != 0;
        }
        return false;
    }

    /* One shot store sync. */
    private async void sync_store() {
        try {
            yield this.store.synchronize(
                false, GLib.Priority.DEFAULT, this.cancellable.cancellable
            );
        } catch (GLib.Error error) {
            warning(error.message);
        }
    }
}