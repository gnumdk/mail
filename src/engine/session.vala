/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Engine session linking Camel objects and Goa objects. */
public class Mail.Engine.Session : Camel.Session {

    private static Session _session;
    public static unowned Session get_default() {
        if (_session == null) {
            _session = new Session();
        }
        return _session;
    }

    /* Blacklisted Camel sources. */
    private static string[] SOURCE_BLACKLIST = {
        "rss", "local", "vfolder"
    };

    private E.SourceRegistry registry;

    /** Create a new session. */
    public Session() {
        Object(
            user_data_dir: Path.build_filename(
                E.get_user_data_dir(), Application.APP_ID
            ),
            user_cache_dir: Path.build_filename(
                E.get_user_cache_dir (),  Application.APP_ID
            )
        );
        Camel.init(E.get_user_data_dir(), false);

        // Let our network monitor handle this
        this.set_network_monitor(null);
        // Fast initial loading
        // If online, store will grab folders list remotelly
        this.set_online(false);
    }

    /** Init E source registry. */
    public async void init() {
        try {
            this.registry = yield new E.SourceRegistry(null);
        } catch (GLib.Error e) {
            GLib.critical(e.message);
        }
    }

    /**
     * Register Gnome Online Accounts object.
     *
     * @param object - {@link Goa.Object}
     */
    public void register_goa_object(Goa.Object object) {
        GLib.List<E.Source> sources = this.registry.list_sources(
            E.SOURCE_EXTENSION_MAIL_ACCOUNT
        );

        foreach (var source in sources) {
            unowned string uid = source.get_uid ();
            if (object.account.identity != source.display_name ||
                is_blacklisted(uid))
                continue;
            var account = get_new_account(source, object, uid);
            if (account != null) {
                Accounts.Manager.get_default().add_account.begin(account);
            }
        }
    }

    /**
     * Unregister Gnome Online Accounts object.
     *
     * @param object - {@link Goa.Object}
     */
    public void unregister_goa_object(Goa.Object object) {
        var accounts = Accounts.Manager.get_default();
        foreach (var account in accounts) {
            if (account.object == object) {
                remove_service(account.store);
                accounts.remove_account(account);
                break;
            }
        }
    }

    /**
     * Instantiates a new {@link Service}.
     *
     * @param uid - Unique identifier string
     * @param protocol - Service protocol
     * @param type - Service type
     */
    public override Camel.Service add_service(string uid, string protocol,
                                              Camel.ProviderType type)
        throws GLib.Error {
        var service = base.add_service(uid, protocol, type);

        if (service is Camel.Service) {
            E.Source? source = this.registry.ref_source(uid);
            unowned string extension_name = E.SourceCamel.get_extension_name(
                protocol
            );
            var extension_source = registry.find_extension(
                source, extension_name
            );
            if (extension_source != null) {
                source = extension_source;
            }

            // Link source and service
            source.camel_configure_service(service);
            source.bind_property(
                "display-name", service,
                "display-name", BindingFlags.SYNC_CREATE
            );
        }

        return service;
    }

    /**
     * Authenticates service.
     *
     * @param service - {@link Service}
     * @param mechanism - SASL mechanism name or null
     * @param cancellable - Optional Cancellable object, or null
     */
    public override bool authenticate_sync(Camel.Service service,
                                           string? mechanism,
                                           GLib.Cancellable? cancellable=null)
        throws GLib.Error {
        // Inspired by evolution source code
        // https://gitlab.gnome.org/GNOME/evolution/-/blob/master/src/mail/e-mail-ui-session.c
        Camel.AuthenticationResult result = Camel.AuthenticationResult.REJECTED;
        // Treat a mechanism name of "none" as null
        if (mechanism == "none") {
            mechanism = null;
        }

        Camel.ServiceAuthType? authtype = null;
        if (mechanism != null)
            authtype = Camel.Sasl.authtype(mechanism);

        // If the SASL mechanism does not involve a user
        // password, then it gets one shot to authenticate
        if (authtype != null && !authtype.need_password) {
            result = service.authenticate_sync(mechanism, cancellable);
             if (result == Camel.AuthenticationResult.REJECTED) {
                throw new Camel.ServiceError.CANT_AUTHENTICATE (
                    "Can't authenticate: %s",
                    mechanism
                );
            }
            return result == Camel.AuthenticationResult.ACCEPTED;
        }

        Accounts.Account account = Accounts.Manager
                                           .get_default()
                                           .get_account_for_service(
            service
        );
        if (account != null) {
            service.set_password(account.token);
            result = service.authenticate_sync(mechanism, cancellable);
        }
        return result == Camel.AuthenticationResult.ACCEPTED;
    }

    /**
     * Obtains the OAuth 2.0 access token for service along with its expiry
     * in seconds from the current time (or 0 if unknown).
     *
     * @param service - {@link Service}
     * @param access_token - Return location for the access token, or null
     * @param expires_in - Return location for the token expiry, or null
     * @param cancellable - Optional Cancellable object, or null
     */
    public override bool get_oauth2_access_token_sync(
        Camel.Service service, out string? access_token,
        out int expires_in, Cancellable? cancellable=null) throws GLib.Error {
        Accounts.Account account = Accounts.Manager
                                           .get_default()
                                           .get_account_for_service(
            service
        );
        if (account != null) {
            if (account.expires_in == 0) {
                refresh_oauth2_token.begin(service, account, cancellable);
                throw new Camel.ServiceError.CANT_AUTHENTICATE (
                    "OAuth token expired"
                );
            }
            access_token = account.token;
            expires_in = account.expires_in;
            return true;
        }
        access_token = null;
        expires_in = 0;
        return false;
    }


    /**
     * Looks up an Source in this by its unique identifier string.
     *
     * @param uid - Unique identifier string
     */
    public E.Source? ref_source(string source_uid) {
        return this.registry.ref_source(source_uid);
    }

    /**
     * Set a default filter driver.
     * @param type - The type of filter (eg, "incoming")
     * @param for_folder - An optional Folder or null
     */
    public override unowned Camel.FilterDriver get_filter_driver(
            string type, Camel.Folder? for_folder) throws Error {
        var filter_driver = new Camel.FilterDriver(this);
        return (Camel.FilterDriver) filter_driver.ref ();
    }

    /* Refresh OAuth2 token. */
    private async void refresh_oauth2_token(
        Camel.Service service, Accounts.Account account,
        GLib.Cancellable cancellable) {
        yield account.refresh_token(cancellable);
        try {
            service.authenticate_sync("XOAUTH2", cancellable);
        } catch (GLib.Error error) {
            warning(error.message);
        }
    }

    /* Get a new {@link Account}. */
    private Accounts.Account? get_new_account(E.Source source,
                                    Goa.Object object,
                                    string uid) {
        unowned var extension = (E.SourceMailAccount) source.get_extension(
            E.SOURCE_EXTENSION_MAIL_ACCOUNT
        );
        try {
            var service = add_service(
                uid, extension.backend_name, Camel.ProviderType.STORE
            );

            // We have an account
            if (service is Camel.OfflineStore) {
                var store = (Camel.OfflineStore) service;
                return new Accounts.Account(object, store);
            }
        } catch (GLib.Error e) {
            GLib.critical(e.message);
        }
        return null;
    }

    /* Check if uid is blacklisted. */
    private bool is_blacklisted(string uid) {
        foreach(var blacklist in SOURCE_BLACKLIST) {
            if(blacklist == uid) {
                return true;
            }
        }
        return false;
    }
}