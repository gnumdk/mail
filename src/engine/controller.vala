/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Controller handling engine workflows. */
public class Mail.Engine.Controller {

    private static Utils.Cancellable _cancellable;
    public static unowned Utils.Cancellable get_cancellable() {
        if (_cancellable == null) {
            _cancellable = new Utils.Cancellable();
        }
        return _cancellable;
    }

    /** Create a new controller. */
    public Controller() {
        this.init.begin();
    }

    /* Init Session and accounts manager. */
    private async void init() {
        yield Session.get_default().init();
        var accounts = Accounts.Manager.get_default();
        accounts.goa_object_available.connect(on_goa_object_available);
        accounts.goa_object_unavailable.connect(on_goa_object_unavailable);
        yield accounts.init();
    }

    /*  Forward Gnome Online Accounts object availability to session. */
    private void on_goa_object_available(Goa.Object object) {
        var accounts = Accounts.Manager.get_default();
        if (accounts.get_account_for_object(object) == null)
            Session.get_default().register_goa_object(object);
    }

    /* Forward Gnome Online Accounts object unavailability to session. */
    private void on_goa_object_unavailable(Goa.Object object) {
        var accounts = Accounts.Manager.get_default();
        if (accounts.get_account_for_object(object) != null)
            Session.get_default().unregister_goa_object(object);
    }
}