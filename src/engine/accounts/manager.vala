/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Manager registered accounts. */
public class Mail.Engine.Accounts.Manager : Gee.ArrayList<Account> {

    private static Manager _manager;
    public static unowned Manager get_default() {
        if (_manager == null) {
            _manager = new Manager();
        }
        return _manager;
    }

    /**
     * Emit the goa_object_available signal.
     *
     * @param object - {@link Goa.Object}
     */
    public signal void goa_object_available(Goa.Object object);

    /**
     * Emit the goa_object_unavailable signal.
     *
     * @param object - {@link Goa.Object}
     */
    public signal void goa_object_unavailable(Goa.Object object);

    /**
     * Emit the account_added signal.
     *
     * @param account - {@link Account}
     */
    public signal void account_added(Account account) {
        NetworkMonitor.get_default().add_account(account);
    }

    /**
     * Emit the account_removed signal.
     *
     * @param account - {@link Account}
     */
    public signal void account_removed(Account account) {
        NetworkMonitor.get_default().remove_account(account);
    }

    /** Init Gnome Online accounts async. */
    public async void init() {
        try {
            var cancellable = Engine.Controller.get_cancellable().cancellable;
            Goa.Client goa = yield new Goa.Client(cancellable);
            goa.account_added.connect(on_goa_account_added);
            goa.account_changed.connect(on_goa_account_changed);
            goa.account_removed.connect(on_goa_account_removed);

            foreach (var object in goa.get_accounts()) {
                on_goa_account_added(object);
            }
        } catch (GLib.Error error) {
            debug("Can't connect to GOA: %s\n", error.message);
        }
    }

    /**
     * Add account.
     *
     * @param account - {@link Account}
     */
    public async void add_account(Account account) {
        var cancellable = Engine.Controller.get_cancellable().cancellable;
        if (yield account.refresh_token(cancellable)) {
            this.add(account);
            account_added(account);
        }
    }

    /**
     * Remove account.
     *
     * @param account - {@link Account}
     */
    public void remove_account(Account account) {
        account_removed(account);
        this.remove(account);
    }

    /**
     * Get account for service.
     *
     * @param service - Camel Service
     *
     * @return {@link Account}
     */
    public Account? get_account_for_service(Camel.Service service) {
        foreach (var account in this) {
            if (account.store == service)
                return account;
        }
        return null;
    }

    /**
     * Get account for Gnome Online Accounts object.
     *
     * @param object - Goa Object
     *
     * @return {@link Account}
     */
    public Account? get_account_for_object(Goa.Object object) {
        foreach (var account in this) {
            if (account.object == object)
                return account;
        }
        return null;
    }

    /**
     * Get account for id.
     *
     * @param id - Account id
     *
     * @return {@link Account}
     */
    public Account? get_account_for_id(string id) {
        foreach (var account in this) {
            if (account.id == id)
                return account;
        }
        return null;
    }

    /* Signal account as available. */
    private void on_goa_account_added(Goa.Object object) {
        on_goa_account_changed(object);
    }

    /* Signal account changes. */
    private void on_goa_account_changed(Goa.Object object) {
        if (object.account.mail_disabled) {
            goa_object_unavailable(object);
        } else {
            goa_object_available(object);
        }
    }

    /* Signal account as unavailable. */
    private void on_goa_account_removed(Goa.Object object) {
        goa_object_unavailable(object);
    }
}