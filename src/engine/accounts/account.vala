/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** An Email account. */
public class Mail.Engine.Accounts.Account : RemoteSync {

    /** Authentication methods supported by the Engine. */
    public enum AuthMethod {
        /** Password-based authentication, such as SASL PLAIN. */
        PASSWORD,
        /** OAuth2-based authentication. */
        OAUTH2;
    }

    /**
     * Emit the online_changed signal.
     *
     * @param online - True if account is online
     */
    public signal void online_changed(bool online);

    /** Account Goa object. */
    public Goa.Object object {
        get; private set;
    }

    /** Account id. */
    public string id {
        owned get {
            return this.object.get_account().id;
        }
    }

    /** Provider (IMAP, Google, ...). */
    public string provider {
        owned get {
            // For IMAP/SMTP, we use mail name
            string name = this.object.get_mail().name;
            if (name != "") {
                return name;
            } else {
                return this.object.get_account().provider_name;
            }
        }
    }

    /**  Name ie something@domain.net. */
    public string name {
        owned get {
            return this.object.get_account().identity;
        }
    }

    /** Authentication token. */
    public string? token {
        get; private set; default = null;
    }

    /** Authentication method. */
    public AuthMethod auth_method {
        get {
            if (this.object.get_oauth2_based() != null) {
                return Account.AuthMethod.OAUTH2;
            }
            return Account.AuthMethod.PASSWORD;
        }
    }

    /** Authentication expiration time. */
    public int expires_in {
        get; private set; default = -1;
    }

    /**
     * Create a new account.
     *
     * @param object - {@link Goa.Object}
     * @param store - {@link Camel.OfflineStore store}
     */
    public Account(Goa.Object object, Camel.OfflineStore store) {
        base(store);
        this.object = object;
    }

    /**
     * Set account online.
     *
     * @param online - True if account is online
     */
    public async void set_online(bool online) {
        var cancellable = Controller.get_cancellable().cancellable;
        try {
            yield this.store.set_online(
                online, GLib.Priority.DEFAULT, cancellable
            );
            if (online) {
                message(
                    "Store %s connected to remote server.",
                    this.store.display_name
                );
            } else {
                message(
                    "Store %s disconnected from remote server.",
                    this.store.display_name
                );
            }
        } catch (GLib.Error error) {
            warning(
                "Can't set %s online: %s\n",
                // API is not explicit about this but looks like it can be
                // null on error.
                this.store.display_name == null ?
                    "unset" : this.store.display_name,
                error.message
            );
        }
        this.online = online;
        online_changed(online);
    }

    /**
     * Refresh authentication token.
     *
     * @param cancellavble - {@link GLib.Cancellable}
     */
    public async bool refresh_token(GLib.Cancellable cancellable) {
        // <https://developer.gnome.org/goa/stable/ch01s03.html>:
        // "First the application should invoke the
        // Account.EnsureCredentials() method […] if the service
        // returns an authorization error (say, the access token
        // expired), the application should call
        // Account.EnsureCredentials() again to e.g. renew the
        // credentials."
        Goa.Account? goa_account = this.object.get_account();
        bool network_available = GLib.NetworkMonitor.get_default()
                                                    .get_network_available();
        if (network_available) {
            try {
                yield goa_account.call_ensure_credentials(cancellable, null);
            } catch (Goa.Error.NOT_AUTHORIZED error) {
                warning(
                    "GOA updating auth failed, retrying: %s", error.message
                );
                try {
                    yield goa_account.call_ensure_credentials(cancellable, null);
                } catch (Goa.Error.NOT_AUTHORIZED error) {
                    warning(
                        "GOA updating auth failed: %s", error.message
                    );
                } catch (GLib.Error error) {
                    warning(error.message);
                }
            } catch (GLib.Error error) {
                warning(error.message);
            }
        }

        string? token = null;
        int expires_in = -1;
        try {
            switch (this.auth_method) {
            case OAUTH2:
                yield this.object.get_oauth2_based().call_get_access_token(
                    cancellable, out token, out expires_in
                );
                break;

            case PASSWORD:
                yield this.object.get_password_based().call_get_password(
                    "imap-password", cancellable, out token
                );
                break;
            default:
                return false;
            }
        } catch (GLib.Error error) {
            warning(
                "GOA failed to get token: %s", error.message
            );
        }
        if (token != null && expires_in != 0) {
            this.token = token;
            this.expires_in = expires_in;
            return true;
        }
        return false;
    }
}