/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Manager conversations for a targeted folder. */
public class Mail.Engine.Conversations.Manager {

    /** Managed conversation model */
    public Model model {
        get;
        private set;
    }

    private const uint DB_LIMIT = 5000;
    private static GLib.HashTable<string, uint64?> db_offset =
            new GLib.HashTable<string, uint64?>(
                str_hash, str_equal
            );

    private uint loading_counter = 0;
    private static GLib.Cancellable cancellable;

    private GLib.HashTable<uint64?, Conversation?> conv_map =
            new GLib.HashTable<uint64?, Conversation?>(
        Utils.Hash.uint64_hash_func,
        Utils.Hash.uint64_equal_func
    );

    private static GLib.HashTable<string, GLib.Queue<Record?>> queues =
        new GLib.HashTable<string, GLib.Queue<Record?>>(str_hash, str_equal);

    private GLib.HashTable<string, Engine.Folders.Folder> folders =
        new GLib.HashTable<string, Engine.Folders.Folder>(str_hash, str_equal);
    private GLib.HashTable<string, Camel.Folder> sent_folders =
        new GLib.HashTable<string, Camel.Folder>(str_hash, str_equal);

    private struct Record {
        string uid;
        string account_id;
        string folder;
        string date;
    }

    /** Create a new manager. */
    public Manager() {
        this.model = new Models.ConversationList.Model();
    }

    /**
     * Populate conversations for folder.
     *
     * @param folder - {@link Engine.Folders.Folder}
     */
    public void populate(Engine.Folders.Folder folder) {
        Manager.cancellable = new GLib.Cancellable();
        foreach (var inner in folder.get_inner_folders()) {
            this.model.add_account(inner.account.id);
        }
        foreach (var inner in folder.get_inner_folders()) {
            add_folder.begin(inner);
        }
        folder.inner_folder_added.connect((folder) => {
            this.model.add_account(folder.account.id);
            add_folder.begin(folder);
        });
        folder.inner_folder_removed.connect((folder) => {
            // TODO
        });
    }

     /**
     * Add message to conversation list.
     *
     * @param account - {@link Engine.Accounts.Account}
     * @param message - {@link Camel.MessageInfo}
     * @param message_folder - {@link Camel.Folder}
     * @param conversation_folder - {@link Camel.Folder}
     */
    public void add_conversation(Engine.Accounts.Account account,
                                 Camel.MessageInfo message,
                                 Camel.Folder message_folder,
                                 Camel.Folder conversation_folder) {
        var references = message.references == null ?
            new GLib.Array<uint64>() : message.references;
        references.prepend_val(message.message_id);

        // Do not create conversations for sent emails
        if (conversation_folder != message_folder) {
            lock (this.conv_map) {
                if (!this.conv_map.contains(message.message_id)) {
                    this.conv_map.set(message.message_id, null);
                }
            }
            return;
        }

        // Search an existing conversation
        Conversation conversation = null;
        lock (this.conv_map) {
            for (uint i=0; i < references.length; i++) {
                var message_id = references.index(i);
                conversation = this.conv_map.get(message_id);
                if (conversation != null) {
                    references.remove_index_fast(i);
                    break;
                }
            }
        }

        // Create a new conversation if none exists
        if (conversation == null) {
            conversation = new Conversation(
                conversation_folder, account
            );
            this.model.add_to_queue(conversation);
            this.model.proceed_queue();
        }

        conversation.add_message(
            new Message(account, message_folder, message)
        );

        // Merge any existing conversation
        Conversation existing_conversation = null;
        foreach (var message_id in references) {
            lock (this.conv_map) {
                existing_conversation = this.conv_map.get(message_id);
            }
            // Looks like references contain duplicated message ids
            if (conversation == existing_conversation) {
                continue;
            // Conversation exists for this id, merge it with new conversation
            } else if (existing_conversation != null) {
                conversation.add_messages(
                    existing_conversation.get_messages()
                );
                this.model.remove(existing_conversation);
            }
            lock (this.conv_map) {
                this.conv_map.set(message_id, conversation);
            }
        }
    }

    /**
     * Load more conversations from DB
     */
    public async void load_more_conversations() {
        new GLib.Thread<void>(null, () => {
            // Wait for threads to finish
            while (this.loading_counter != 0) {
                GLib.Thread.usleep(100000);
            }
            GLib.Idle.add(load_more_conversations.callback);
        });
        yield;

        this.loading_counter++;
        foreach (var account_id in this.folders.get_keys()) {
            if (Manager.db_offset.get(account_id) == null) {
                continue;
            }
            var folder = this.folders.get(account_id);
            var sent_folder = this.sent_folders.get(account_id);
            new GLib.Thread<void>(null, () => {
                add_conversations(folder.account, folder.camel_folder, sent_folder);
                GLib.Idle.add(load_more_conversations.callback);
            });
            yield;
        }
        this.loading_counter--;
    }

    /**
     * Cancel conversation loading.
     */
    public async void cancel() {
        Manager.cancellable.cancel();
        new GLib.Thread<void>(null, () => {
            // Wait for threads to finish
            while (this.loading_counter != 0) {
                GLib.Thread.usleep(100000);
            }
            GLib.Idle.add(cancel.callback);
        });
        yield;
        yield this.model.cancel_queue();

        lock (this.conv_map) {
            this.conv_map.remove_all();
            Manager.queues.remove_all();
            Manager.db_offset.remove_all();
            this.folders.remove_all();
            this.sent_folders.remove_all();
        }
    }

    /* Add folder to manager. */
    private async void add_folder(Engine.Folders.Folder folder) {
        Camel.Folder sent_folder = yield Utils.Folders.get_camel_folder(
            folder.account.store,
            Utils.Folders.get_name_for_sent(folder.account),
            Manager.cancellable
        );

        // We prepare loading for this folder
        this.folders.set(folder.account.id, folder);
        this.sent_folders.set(folder.account.id, sent_folder);
        Manager.queues.set(folder.account.id, new GLib.Queue<Record?>());
        Manager.db_offset.set(folder.account.id, 0);

        this.loading_counter++;
        new GLib.Thread<void>(null, () => {
            add_conversations(folder.account, folder.camel_folder, sent_folder);
            GLib.Idle.add(add_folder.callback);
        });
        yield;
        this.loading_counter--;
    }

    /* Add conversations to manager. */
    private void add_conversations(Engine.Accounts.Account account,
                                   Camel.Folder folder,
                                   Camel.Folder sent_folder) {
        // We want messages ordered what is not possible with folder_get_uids().
        Camel.DB db = account.store.get_db();
        uint64 offset = (uint64) Manager.db_offset.get(account.id);
        try {
            db.prepare_message_info_table(folder.full_name);
            db.select(
                """SELECT uid, %s as account, 'main' as folder, dsent FROM %s
                   UNION
                   SELECT uid, %s as account, 'sent' as folder, dsent FROM %s
                   ORDER BY dsent DESC LIMIT %s OFFSET %s""".printf(
                    Camel.DB.sqlize_string(account.id),
                    Camel.DB.sqlize_string(folder.full_name),
                    Camel.DB.sqlize_string(account.id),
                    Camel.DB.sqlize_string(sent_folder.full_name),
                    "%ld".printf(Manager.DB_LIMIT),
                    "%lld".printf(offset)
                ),
                (c, n) => {
                    Record record = Record() {
                        uid = c[0],
                        account_id = c[1],
                        folder = c[2],
                        date = c[3]
                    };
                    unowned GLib.Queue<Record?> queue = Manager.queues.get(
                        record.account_id
                    );
                    queue.push_tail(record);
                    return Manager.cancellable.is_cancelled() ? -1: 0;
                }
            );
            if (Manager.queues.get(account.id).length == Manager.DB_LIMIT) {
               Manager.db_offset.set(account.id, offset + Manager.DB_LIMIT);
            } else {
                Manager.db_offset.set(account.id, null);
            }
        } catch (GLib.Error error) {
            warning(error.message);
        }

        // For each message, build a conversation.
        // Reference messages belong to same conversation.
        unowned GLib.Queue<Record?> queue = Manager.queues.get(account.id);
        while (!Manager.cancellable.is_cancelled() && !queue.is_empty()) {
            Record record = queue.pop_head();
            Camel.Folder message_folder;
            // Search for an existing conversation
            // for message and its references
            if (record.folder == "main") {
                message_folder = this.folders.get(record.account_id).camel_folder;
            } else {
                message_folder = this.sent_folders.get(record.account_id);
            }
            var message = message_folder.get_message_info(record.uid);
            if (message != null) {
                add_conversation(account, message, message_folder, folder);
            }
        }
    }
}