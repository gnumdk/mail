/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Represent a message in a conversation. */
public class Mail.Engine.Conversations.Message : GLib.Object {

    /** Message account. */
    public Engine.Accounts.Account account { get; construct; }
    /** Message information. */
    public Camel.MessageInfo info { get; construct; }
    /** Message mime message. */
    public Camel.MimeMessage mime { get; private set; }
    /** Message folder. */
    public Camel.Folder folder { get; construct; }

    public bool synced { get; private set; default = false; }

    /**
     * Create a new message.
     *
     * @param account - {@link Engine.Accounts.Account}
     * @param folder - {@link Camel.Folder}
     * @param message_info - {@link Camel.MessageInfo}
     */
    public Message(Engine.Accounts.Account account,
                   Camel.Folder folder,
                   Camel.MessageInfo message_info) {
        Object(account: account, folder: folder, info: message_info);
    }

    /**
     * Sync message to cache.
     *
     * @param cancellable - {@link GLib.Cancellable}
     *
     * @return true if message has been synced. If message was already synced,
     * false is returned.
     */
    public async bool sync(GLib.Cancellable cancellable) {
        if (this.synced) {
            return false;
        }

        try {
            this.mime = yield this.folder.get_message(
                this.info.uid, GLib.Priority.DEFAULT, cancellable
            );
            this.synced = true;
        } catch (GLib.Error error) {
            warning(error.message);
        }
        return this.synced;
    }
}