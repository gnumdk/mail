/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Model for conversations. */
public abstract class Mail.Engine.Conversations.Model :
                                                   GLib.Object, GLib.ListModel {

    /** Emitted when all accounts have at least one message added. */
    public signal void all_accounts_added();

    /** Conversations length */
    private uint length = 0;

    /* Conversations in model. */
    private GLib.Sequence<Engine.Conversations.Conversation> conversations =
        new GLib.Sequence<Engine.Conversations.Conversation>();

    /* Account that should be populated.
     *  We use this to detect all_account_added() emittion.
     */
    private Gee.ArrayList<string> accounts = new Gee.ArrayList<string>();

    /** Create a new model. */
    protected Model() {
        Object();
    }

    /**
     * Add an account to model.
     * You need to set any account that will create conersations before
     * populating the model.
     *
     * @param account_id - {@link string}
     */
    public void add_account(string account_id) {
        lock (this.accounts) {
            this.accounts.add(account_id);
        }
    }

    /**
     * Get the item at position.
     *
     * @param position - Position of the item to fetch
     *
     * @return an object
     */
    public GLib.Object? get_item(uint position) {
        lock (this.conversations) {
            var iter = this.conversations.get_iter_at_pos((int) position);
            return iter.is_end() ? null : iter.get();
        }
    }

    /**
     * Gets the type of the items in liststore.
     *
     * @return GLib.Type
     */
    public GLib.Type get_item_type() {
        return typeof(Engine.Conversations.Conversation);
    }

    /**
     * Gets the number of items in liststore.
     */
    public uint get_n_items() {
        return this.length;
    }

    /**
     * Add conversation to model queue.
     *
     * @param conversation - {@link Engine.Conversations.Conversation}
     */
    public abstract void add_to_queue(
                                Engine.Conversations.Conversation conversation);

    /**
     * Remove conversation from model.
     *
     * @param conversation - {@link Engine.Conversations.Conversation}
     */
    public abstract void remove(Engine.Conversations.Conversation conversation);

    /**
     * Proceed the queue.
     */
    public abstract void proceed_queue();

    /**
     * Cancel queue processing.
     */
    public abstract async void cancel_queue();

    /**
     * Add from model
     */
    protected void add_conversation(Conversation conversation) {
        lock (this.conversations) {
            var iter = this.conversations.insert_sorted(
                conversation,
                (a, b) => {
                    var item_a = (Engine.Conversations.Conversation) a;
                    var item_b = (Engine.Conversations.Conversation) b;
                    return (int) item_b.timestamp -
                           (int) item_a.timestamp;
                }
            );
            items_changed(iter.get_position(), 0, 1);
            this.length++;
        }
        lock (this.accounts) {
            if (this.accounts.contains(conversation.account.id)) {
                this.accounts.remove(conversation.account.id);
                if (this.accounts.size == 0) {
                    GLib.Timeout.add(100, () => {
                        all_accounts_added();
                        return Source.REMOVE;
                    });
                }
            }
        }
    }

    /**
     * Remove conversation from model
     *
     * This function is thread safe.
     * If called inside GLib.Thread, be sure to wait thread at GLib.Priority.LOW
     */
    protected void remove_conversation(Conversation conversation) {
        lock (this.conversations) {
            var iter = this.conversations.get_begin_iter();
            while (!iter.is_end()) {
                if (iter.get() == conversation) {
                    uint position = iter.get_position();
                    iter.remove();
                    this.length--;
                    GLib.Idle.add(() => {
                        lock (this.conversations) {
                            items_changed(position, 1, 0);
                        }
                        return Source.REMOVE;
                    }, GLib.Priority.HIGH);
                    break;
                }
                iter = iter.next();
            }
        }
    }

    /**
     * Reset model conversations.
     *
     * This function is thread safe.
     * If called inside GLib.Thread, be sure to wait thread at GLib.Priority.LOW
     */
    protected void reset_conversations() {
        lock (this.accounts) {
            this.accounts = new Gee.ArrayList<string>();
        }
        lock (this.conversations) {
            uint length = this.conversations.get_length();
            var iter = this.conversations.get_begin_iter();
            iter.remove_range(this.conversations.get_end_iter());
            this.length = 0;
            GLib.Idle.add(() => {
                lock (this.conversations) {
                    items_changed(0, length, 0);
                }
                return Source.REMOVE;
            }, GLib.Priority.HIGH);
        }
    }
}