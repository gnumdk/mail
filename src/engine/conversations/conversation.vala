/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

// For demos
// string string_random(int length = 8,
//                      string charset = "abcdefghijklmnopqrstuvwxyz"){
//     string random = "";

//     for(int i=0 ; i<length ; i++){
//         int random_index = Random.int_range(0,charset.length);
//         string ch = charset.get_char(
//             charset.index_of_nth_char(random_index)
//         ).to_string();
//         random += ch;
//     }

//     return random;
// }

/** A conversation regrouping multiple mails by references. */
public class Mail.Engine.Conversations.Conversation : GLib.Object {

    /** Conversation subject, first message subject. */
    public string subject {
        owned get {
            // return string_random(8);
            var message = this.messages.last();
            if (message == null) {
                return "";
            }
            return Utils.Mail.strip_prefixes(message.info.subject);
        }
    }

    /** All senders for this conversation. */
    public string? from {
        owned get {
            // return "%s@%s.%s".printf(
            //     string_random(8), string_random(5), string_random(2)
            // );
            return Utils.Mail.get_senders_for_messages(this.messages);
        }
    }

    /** Last message preview. */
    public string preview {
        owned get {
            // return string_random(80);
            var message = this.messages.last();
            if (message == null) {
                return "";
            }
            var preview = message.info.get_preview();
            if (preview == null) {
                if (!message.synced) {
                    this.account.add_message_to_sync_queue(message);
                    message.notify["synced"].connect(() => {
                        GLib.Idle.add(() => {
                            notify_property("preview");
                            return GLib.Source.REMOVE;
                        });
                    });
                }
                return "";
            }
            return preview;
        }
    }

    /** Last message sent date. */
    public string date {
        owned get {
            var date = new DateTime.from_unix_utc(this.timestamp);
            var now = new  DateTime.now();
            if (now.get_year() != date.get_year()) {
                return date.format("%x");
            } else if (now.get_day_of_month() != date.get_day_of_month()) {
                return date.to_local().format("%d %b");
            } else {
                return date.to_local().format("%H:%M");
            }
        }
    }

    /** Last message timestamp. */
    public int64 timestamp {
        get {
            var message = this.messages.last();
            if (message == null) {
                return 0;
            }
            return message.info.date_sent;
        }
    }

    /** Message count. */
    public uint64 count {
        get {
            return this.messages.size;
        }
    }

    /** Is message count important. */
    public bool count_important {
        get {
            return this.messages.size > 1;
        }
    }

    /** Conversation related account. */
    public Accounts.Account account { get; private set; }

    private Gee.ConcurrentSet<Message> messages = new Gee.ConcurrentSet<Message>(
        (a, b) => {
            var item_a = (Message) a;
            var item_b = (Message) b;
            return (int) (item_a.info.date_sent - item_b.info.date_sent);
        }
    );

    private Camel.Folder folder;

    /**
     * Create a new conversation for this account.
     *
     * @param folder - {@link Camel.Folder}
     * @param account - {@link Engine.Accounts.Account}
     */
    public Conversation(Camel.Folder folder, Accounts.Account account) {
        Object();
        this.folder = folder;
        this.account = account;
    }

    ~Conversation() {
        foreach (var message in this.messages) {
            this.account.remove_message_from_sync_queue(message);
        }
    }

    /** Add a new message to conversation. */
    public void add_message(Message message) {
        this.messages.add(message);
        notify_properties();
    }

    /** Add all messages to conversation. */
    public void add_messages(Gee.Collection<Message> messages)  {
        this.messages.add_all(messages);
        notify_properties();
    }

    /** Get all messages from conversation. */
    public Gee.Collection<Message> get_messages() {
        return this.messages;
    }

    private void notify_properties() {
        GLib.Idle.add(() => {
            notify_property("preview");
            notify_property("subject");
            notify_property("date");
            notify_property("from");
            return GLib.Source.REMOVE;
        });
    }
}