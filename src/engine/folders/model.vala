/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Folder list base model. */
public abstract class Mail.Engine.Folders.Model<L> {

    public Gtk.TreeListModel root_model { get; protected set; }

    protected L root_store;

    protected Gee.HashMap<string, L> leaf_stores;

    /**
     * Create a new model
     *
     * @param liststore - L
     *
     */
    protected Model(L liststore) {
        this.root_store = liststore;
        this.leaf_stores = new Gee.HashMap<string, L>();

        this.root_model = new Gtk.TreeListModel(
            (GLib.ListModel) this.root_store,
            false, false,
            get_leaf_store
        );
    }

   /**
     * Get a new model for leaf.
     *
     * @param object - @{Engine.Folders.Folder}
     */
    protected abstract GLib.ListModel? get_leaf_store(GLib.Object object);
}