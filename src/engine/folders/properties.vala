/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Folder properties. Many properties are account related (draft name, ...). */
public class Mail.Engine.Folders.Properties {

    private static IconNameMap icon_name_map = new IconNameMap();

    /**
     * Common folder types
     */
    public enum Type {
        /** This folder contains all the messages. */
        ALL,
        /** The folder is an inbox folder. */
        INBOX,
        /** The folder contains starred messages. */
        STARRED,
        /** The folder contains important messages. */
        IMPORTANT,
        /** The folder is an outbox folder. */
        OUTBOX,
        /** The folder contains sent messages. */
        SENT,
        /** The folder contains junks. */
        JUNKS,
        /** The folder contains drafts. */
        DRAFTS,
        /** The folder contains templates. */
        TEMPLATES,
        /** The folder contains archived messages. */
        ARCHIVES,
        /** The folder is a trash folder. */
        TRASH,
        /** Not a folder but a tag (GMail for example). */
        TAG,
        /** Others folders. */
        OTHERS;
    }

    private Maps.Base folder_type_map;

    private string archives_name;
    private string sent_name;
    private string drafts_name;
    private string templates_name;

    private Engine.Accounts.Account account;

    /**
     * Create properties for account.
     *
     * @param account - Account we want to get properties from
     */
    public Properties(Engine.Accounts.Account account) {
        this.account = account;
        this.archives_name = Utils.Folders.get_name_for_archives(account);
        this.sent_name = Utils.Folders.get_name_for_sent(account);
        this.drafts_name = Utils.Folders.get_name_for_drafts(account);
        this.templates_name = Utils.Folders.get_name_for_templates(account);

        switch (this.account.provider) {
        case "Google":
            this.folder_type_map = new Maps.Google();
            break;
        default:
            this.folder_type_map = new Maps.Imap();
            break;
        }
    }

    /**
     * Get translated name for type.
     *
     * @param type - Folder type
     *
     * @return folder name
     */
    public string? get_folder_translated_name(Type type) {
        return this.folder_type_map.get_translated(type);
    }

    /**
     * Get folder type.
     *
     * @param folder_info - Folder info
     *
     * @return folder type
     */
    public async Type get_folder_type(Camel.FolderInfo folder_info) {
        switch (folder_info.flags & Camel.FOLDER_TYPE_MASK) {
        case Camel.FolderInfoFlags.TYPE_INBOX:
            return Type.INBOX;
        case Camel.FolderInfoFlags.TYPE_ARCHIVE:
            return Type.ARCHIVES;
        case Camel.FolderInfoFlags.TYPE_DRAFTS:
            return Type.DRAFTS;
        case Camel.FolderInfoFlags.TYPE_JUNK:
            return Type.JUNKS;
        case Camel.FolderInfoFlags.TYPE_OUTBOX:
            return Type.OUTBOX;
        case Camel.FolderInfoFlags.TYPE_TRASH:
            return Type.TRASH;
        case Camel.FolderInfoFlags.TYPE_SENT:
            return Type.SENT;
        case Camel.FolderInfoFlags.FLAGGED:
            return Type.STARRED;
        default:
            return yield get_type_for_name(folder_info.display_name);
        }
    }

    /**
     * Get folder unread count for type.
     *
     * @param type - Folder type
     * @param unread_count - Known unread count
     *
     * @return folder name
     */
    public static uint get_unread_count(Type type, uint unread_count) {
        switch (type) {
        case Type.TRASH:
            return 0;
        default:
            return unread_count;
        }
    }

    /**
     * Get icon name for type.
     *
     * @param type - Folder type
     *
     * @return folder icon name
     */
    public static string get_icon_name(Type type) {
        return icon_name_map.get(type);
    }

    /*
     * Get folder type for name.
     */
    private async Type get_type_for_name(string name) {
        if (this.archives_name != null && name == this.archives_name) {
            return Type.ARCHIVES;
        } else if (this.sent_name != null && name == this.sent_name) {
            return Type.SENT;
        } else if (this.templates_name != null && name == this.templates_name) {
            return Type.TEMPLATES;
        } else if (this.drafts_name != null && name == this.drafts_name) {
            return Type.DRAFTS;
        }

        return this.folder_type_map.get_folder_type(name);
    }
}