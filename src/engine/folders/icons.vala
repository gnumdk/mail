/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Map associating folder type with icon name. */
private class Mail.Engine.Folders.IconNameMap :
            Gee.HashMap<Properties.Type, string> {

    /** Create a new icon name map. */
    public IconNameMap() {
        base();
        this.set(Properties.Type.INBOX, "mail-inbox-symbolic");
        this.set(Properties.Type.ARCHIVES, "mail-archive-symbolic");
        this.set(Properties.Type.DRAFTS, "mail-drafts-symbolic");
        this.set(Properties.Type.JUNKS, "dialog-warning-symbolic");
        this.set(Properties.Type.OUTBOX, "mail-outbox-symbolic");
        this.set(Properties.Type.TRASH, "user-trash-symbolic");
        this.set(Properties.Type.SENT, "mail-sent-symbolic");
        this.set(Properties.Type.STARRED, "starred-symbolic");
        this.set(Properties.Type.TAG, "tag-symbolic");
        this.set(Properties.Type.IMPORTANT, "task-due-symbolic");
        this.set(Properties.Type.TEMPLATES, "folder-templates-symbolic");
        this.set(Properties.Type.OTHERS, "folder-symbolic");
    }
}