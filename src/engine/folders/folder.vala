/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** IMAP folder base class. */
public abstract class Mail.Engine.Folders.Folder : GLib.Object {

    /** An inner folder has been added. */
    public signal void inner_folder_added(Folder folder);

    /** An inner folder has been removed. */
    public signal void inner_folder_removed(Folder folder);

    /** Camel folder hold by this folder. */
    public virtual Camel.Folder camel_folder { get; protected set; }

    /** True if folder has children. */
    public virtual bool has_children { get; set; }

    /** Folder account. */
    public virtual Accounts.Account? account { get; set; default=null; }

    /** Folder IMAP path normalized to UNIX path (> replaced by /). */
    public virtual string path { get; protected set; }

    /** Folder name. */
    public virtual string name { get; protected set; }

    /** Folder icon name. */
    public virtual string icon_name { get; protected set; }

    /** Folder total emails count. */
    public virtual uint total { get; protected set; }

    /** Folder unread emails count. */
    public virtual uint unread { get; protected set; }

    /** Folder type. */
    public virtual Properties.Type folder_type {
        get; protected set;
    }

    /** True if folder is expanded (showing its children). */
    public virtual bool expanded {
        get {
            return Settings.Cache.get_default().get_folder_expanded(this);
        }
        set {
            Settings.Cache.get_default().set_folder_expanded(this, value);
        }
    }

    /**
     * Refresh folder info.
     *
     * @param cancellable - {@link GLib.Cancellable}
     */
    public virtual async void refresh(GLib.Cancellable cancellable)
            throws GLib.Error {
        debug("Refreshing: %s/%s", this.account.name, this.path);
        yield this.camel_folder.refresh_info(
            GLib.Priority.DEFAULT, cancellable
        );
    }

    /**
     * Get inner folders
     *
     * @return list of inner folders
     */
    public abstract Gee.ArrayList<Folder> get_inner_folders();
}