/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Easily get information from folders. */
public class Mail.Engine.Folders.Getter : Gee.ArrayList<Camel.FolderInfo> {

    /**
     * Get folder info.
     *
     * @param store - {@link Camel.Store} we want to get info from
     * @param path - Base folder path
     * @param flags - {@Camel.StoreGetFolderInfoFlags} to control behavior
     * @param cancellable - {@link GLib.Cancellable}
     *
     * @return a Getter object
     */
    public static async Getter info(Camel.Store store, string? path,
                                    Camel.StoreGetFolderInfoFlags flags,
                                    GLib.Cancellable cancellable) {
        var info = new Getter();

        Camel.FolderInfo? folder_info = yield Utils.Folders.get_folder_info(
            store, path, flags, cancellable
        );

        // Invalid path
        if (folder_info == null) {
            return info;
        // If / we return siblings else children
        } else if (path != null) {
            folder_info = folder_info.child;
        }

        yield info.populate_info(folder_info);

        return info;
    }

    /*
     * Populate folder information transforming Camel.FolderInfo list to
     * an array (ignoring children).
     */
    private async void populate_info(Camel.FolderInfo? folder_info) {
        if (folder_info == null)
            return;

        this.add(folder_info);

        if (folder_info.next != null) {
            yield populate_info(folder_info.next);
        }
    }
}