/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/**
 * Map associating folder name with folder type.
 *
 * Subclasses should handle translated map.
 */
private class Mail.Engine.Folders.Maps.Base {

    /** Folder names map. */
    protected Gee.HashMap<Properties.Type, Gee.Collection<string>> folders =
        new Gee.HashMap<Properties.Type, Gee.Collection<string>>();

    /** Folder names translation map. */
    protected Gee.HashMap<Properties.Type, string> translated_folders =
          new Gee.HashMap<Properties.Type, string>();

    /** Default folder type. */
    protected Properties.Type default_folder_type = Properties.Type.OTHERS;

    /**
     * Create a new map for default folders.
     */
    public Base() {
        this.folders.set(
            Properties.Type.INBOX, new Gee.ArrayList<string>.wrap({
                "Inbox"
            })
        );
        this.folders.set(
            Properties.Type.ARCHIVES, new Gee.ArrayList<string>.wrap({
                "Archives", "Archive"
            })
        );
        this.folders.set(
            Properties.Type.DRAFTS, new Gee.ArrayList<string>.wrap({
                "Drafts", "Draft"
            })
        );
        this.folders.set(
            Properties.Type.JUNKS, new Gee.ArrayList<string>.wrap({
                "Junks", "Junk"
            })
        );
        this.folders.set(
            Properties.Type.OUTBOX, new Gee.ArrayList<string>.wrap({
                "Outbox"
            })
        );
        this.folders.set(
            Properties.Type.TRASH, new Gee.ArrayList<string>.wrap({
                "Trash"
            })
        );
        this.folders.set(
            Properties.Type.SENT, new Gee.ArrayList<string>.wrap({
                "Sent"
            })
        );
        this.folders.set(
            Properties.Type.STARRED, new Gee.ArrayList<string>.wrap({
                "Starred"
            })
        );
        this.folders.set(
            Properties.Type.IMPORTANT, new Gee.ArrayList<string>.wrap({
                "Important"
            })
        );
        this.folders.set(
            Properties.Type.TEMPLATES, new Gee.ArrayList<string>.wrap({
                "Templates", "Template"
            })
        );
    }

    /**
     * Get translated folder name for type.
     *
     * @param type - {@link Properties.Type}
     *
     * @return folder name
     */
    public string? get_translated(Properties.Type type) {
        var str = this.translated_folders.get(type);
        if (str != null)
            return this.translated_folders.get(type).split(";")[0];
        return null;
    }

    /**
     * Get folder type for name.
     *
     * @param name - Folder name
     *
     * @return folder type
     */
    public Properties.Type get_folder_type(string name) {
        foreach (var key in this.folders.keys) {
            if (this.folders.get(key).contains(name))
                return key;
        }

        foreach (var key in this.translated_folders.keys) {
            var str = this.translated_folders.get(key);
            if (str != null)
                foreach (var item in str.split(";"))
                    if (name.down() == item.down())
                        return key;
        }
        return this.default_folder_type;
    }
}