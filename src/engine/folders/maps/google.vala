/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/**
 * Map associating folder name with folder type.
 *
 * This map contains Google translated labels.
 */
private class Mail.Engine.Folders.Maps.Google : Mail.Engine.Folders.Maps.Base {

    /**
     * Create a new map for Google folders
     */
    public Google() {
        base();

         // Translators: GMail label in your language
        this.translated_folders.set(Properties.Type.INBOX, _("Inbox"));
        // Translators: GMail label in your language
        this.translated_folders.set(Properties.Type.ARCHIVES, _("Archives"));
        // Translators: GMail label in your language
        this.translated_folders.set(Properties.Type.DRAFTS, _("Drafts"));
        // Translators: GMail label in your language
        this.translated_folders.set(Properties.Type.JUNKS, _("Junks"));
        // Translators: GMail label in your language
        this.translated_folders.set(Properties.Type.OUTBOX, _("Outbox"));
        // Translators: GMail label in your language
        this.translated_folders.set(Properties.Type.TRASH, _("Trash"));
        // Translators: GMail label in your language
        this.translated_folders.set(Properties.Type.SENT, _("Sent"));
        // Translators: GMail label in your language
        this.translated_folders.set(Properties.Type.STARRED, _("Starred"));
        // Translators: GMail label in your language
        this.translated_folders.set(Properties.Type.IMPORTANT, _("Important"));
        // Translators: GMail label in your language
        this.translated_folders.set(Properties.Type.TEMPLATES, _("Templates"));

        this.default_folder_type = Properties.Type.TAG;
    }
}