/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/**
 * Map associating folder name with folder type.
 *
 * This map contains common IMAP translated labels.
 */
private class Mail.Engine.Folders.Maps.Imap : Mail.Engine.Folders.Maps.Base {

    /** Create a new map for Imap folders. */
    public Imap() {
        base();

        // Translators: IMAP folder in your language
        // You can put multiple values separated by ;
        this.translated_folders.set(Properties.Type.INBOX, _("Inbox"));
        // Translators: IMAP folder in your language
        // You can put multiple values separated by ;
        this.translated_folders.set(Properties.Type.ARCHIVES, _("Archives"));
        // Translators: IMAP folder in your language
        // You can put multiple values separated by ;
        this.translated_folders.set(Properties.Type.DRAFTS, _("Drafts"));
        // Translators: IMAP folder in your language
        // You can put multiple values separated by ;
        this.translated_folders.set(Properties.Type.JUNKS, _("Junks"));
        // Translators: IMAP folder in your language
        // You can put multiple values separated by ;
        this.translated_folders.set(Properties.Type.OUTBOX, _("Outbox"));
        // Translators: IMAP folder in your language
        // You can put multiple values separated by ;
        this.translated_folders.set(Properties.Type.TRASH, _("Trash"));
        // Translators: IMAP folder in your language
        // You can put multiple values separated by ;
        this.translated_folders.set(Properties.Type.SENT, _("Sent"));
        // Translators: IMAP folder in your language
        // You can put multiple values separated by ;
        this.translated_folders.set(Properties.Type.STARRED, _("Starred"));
        // Translators: IMAP folder in your language
        // You can put multiple values separated by ;
        this.translated_folders.set(Properties.Type.IMPORTANT, _("Important"));
        // Translators: IMAP folder in your language
        // You can put multiple values separated by ;
        this.translated_folders.set(Properties.Type.TEMPLATES, _("Templates"));
    }
}