/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** ListView showing conversations. */
[GtkTemplate (ui = "/org/adishatz/Mail/conversation-list-view.ui")]
public class Mail.ConversationList.View : Adw.Bin {

    public signal void conversation_list_scroll_percent(uint percent);

    [GtkChild] private unowned Gtk.SingleSelection model;
    [GtkChild] private unowned Gtk.ScrolledWindow scrolled;

    uint previous_scroll_percentage = 0;
    uint scroll_source_id = 0;

    /**
     * Set view model.
     *
     * @param model - {@link Engine.Conversations.Model}
     */
    public void set_model(Engine.Conversations.Model model) {
        this.model.set_model(model);
    }

    /**
     * Select first item
     */
    public void select_first_item() {
        this.model.set_selected(0);
        this.scrolled.vadjustment.set_value(0.0);
    }

    /* Reset previous scroll percentage */
    [GtkCallback]
    private void on_adjustment_changed(Gtk.Adjustment adj) {
        this.previous_scroll_percentage = 0;
    }

    /* Load more conversations */
    [GtkCallback]
    private void on_adjustment_value_changed(Gtk.Adjustment adj) {
        uint scroll_percentage = (uint) (
            (adj.value + adj.page_size) * 100 / adj.upper
        );

        if (this.scroll_source_id != 0) {
            GLib.Source.remove(this.scroll_source_id);
        }

        this.scroll_source_id = GLib.Timeout.add(250, () => {
            if (this.previous_scroll_percentage != scroll_percentage) {
                conversation_list_scroll_percent(scroll_percentage);
                this.previous_scroll_percentage = scroll_percentage;
            }
            this.scroll_source_id = 0;
            return Source.REMOVE;
        });
    }
}