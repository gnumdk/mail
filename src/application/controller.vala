/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/**
 * Controller handling application workflows.
 */
public class Mail.Controller : Mail.Engine.Controller {

    /** Select first item in list. */
    public signal void select_first_item();

    private static Controller _controller;
    public static Controller get_default() {
        if (_controller == null) {
            _controller = new Controller();
        }
        return _controller;
    }

    /** The aggregated model for folder list. */
    public Models.FolderList.AggregatedModel folder_list_aggregated_model {
        get;
        default = new Models.FolderList.AggregatedModel();
    }

    /** Conversation manager populating conversation view. */
    public Engine.Conversations.Manager conversation_manager {
        get;
        default = new Engine.Conversations.Manager();
    }

    /* Folder list model populating folders sidebar view. */
    private Gee.HashMap<Engine.Accounts.Account, Models.FolderList.Model>
        folder_list_models =
        new Gee.HashMap<Engine.Accounts.Account, Models.FolderList.Model>();

    /* Prevent multiple cancellation */
    private uint cancel_counter = 0;

    /** Create a new controller. */
    public Controller() {
        base();
        var accounts = Engine.Accounts.Manager.get_default();
        // Be sure engine has finished handling new account
        accounts.account_added.connect_after(on_account_added);
        accounts.account_removed.connect(on_account_removed);

        this.conversation_manager.model.all_accounts_added.connect(() => {
            select_first_item();
        });
    }

    /**
     * Get folder list model for account.
     *
     * @param account - {@link Engine.Accounts.Account}
     *
     * @return account folder list model
     */
    public Models.FolderList.Model? get_folder_list_model(Engine.Accounts.Account account) {
        return this.folder_list_models.get(account);
    }

    /**
     * Load conversations for folder.
     *
     * @param folder - {@link Engine.Folders.Folder}
     */
    public async void load_conversations(Engine.Folders.Folder folder) {
        lock (this.cancel_counter) {
            // Not needed if another cancel already running
            bool cancel_needed = this.cancel_counter == 0;

            this.cancel_counter++;

            new GLib.Thread<void>(null, () => {
                // Wait for previous cancel to finish
                while (this.cancel_counter > 1) {
                    GLib.Thread.usleep(100000);
                }
                GLib.Idle.add(load_conversations.callback);
            });
            yield;

            if (cancel_needed) {
                yield this.conversation_manager.cancel();
            }

            // Do not populate if another folder has been selected
            if (this.cancel_counter == 1) {
                this.conversation_manager.populate(folder);
            }
            this.cancel_counter--;
        }
    }

    /* On account added, add folder list model. */
    private void on_account_added(Engine.Accounts.Account account) {
        var model = new Models.FolderList.Model(account);
        this.folder_list_aggregated_model.add_model(model);
        this.folder_list_models.set(account, model);
        model.sync.begin();
    }

    /* On account removed, remove folder list model. */
    private void on_account_removed(Engine.Accounts.Account account) {
        var model = this.folder_list_models.get(account);
        model.cancel();
        this.folder_list_aggregated_model.remove_model(model);
        this.folder_list_models.unset(account);
    }
}