/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** The application's window. */
[GtkTemplate (ui = "/org/adishatz/Mail/application-window.ui")]
public class Mail.Window : Adw.ApplicationWindow {

    [GtkChild] private unowned HeaderBar.Accounts.Button accounts_button;
    [GtkChild] private unowned FolderList.View folder_list_view;
    [GtkChild] private unowned ConversationList.View conversation_list_view;

    /**
     * Create a new windows for app.
     *
     * @param app - Targeted application
     */
    public Window(Application app) {
        Object(application: app);
        var controller = Controller.get_default();
        this.conversation_list_view.set_model(
            controller.conversation_manager.model
        );
        this.accounts_button.init();

        controller.select_first_item.connect(() => {
            this.conversation_list_view.select_first_item();
        });
        this.conversation_list_view.conversation_list_scroll_percent.connect(
            (percent) => {
                if (percent > 50) {
                    controller.conversation_manager
                              .load_more_conversations.begin();
                }
        });
    }

    /* On active account change, set folder list model. */
    [GtkCallback]
    private void on_active_account_changed() {
        Engine.Folders.Model model;
        if (this.accounts_button.active == null) {
            model = Controller.get_default().folder_list_aggregated_model;
        } else {
            model = Controller.get_default().get_folder_list_model(
                this.accounts_button.active
            );
        }
        this.folder_list_view.set_model(model);
    }
}