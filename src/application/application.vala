/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** The application's main point of entry and desktop integration. */
public class Mail.Application : Adw.Application {

    public const string APP_ID = _APP_ID;
    public const string RESOURCE_BASE_PATH = "/org/adishatz/Mail";

    public const string ACTION_OPEN_ACCOUNTS = "accounts";

    // Determines if Mail appears to be running under Flatpak
    public bool is_flatpak_sandboxed { get; private set; }

    private const ActionEntry[] APP_ACTIONS = {
        { ACTION_OPEN_ACCOUNTS, on_open_accounts }
    };

    /** Create a new application. */
    public Application() {
        Object(
            application_id: APP_ID,
            resource_base_path: RESOURCE_BASE_PATH,
            flags: (
                GLib.ApplicationFlags.DEFAULT_FLAGS
            )
        );
        this.add_action_entries(APP_ACTIONS, this);
        this.is_flatpak_sandboxed = GLib.FileUtils.test(
            "/.flatpak-info", EXISTS
        );
        this.shutdown.connect(on_shutdown);
    }

    /** Activate main window. */
    public override void activate() {
        base.activate();
        var win = this.active_window;
        if (win == null) {
            win = new Window(this);
        }
        win.present ();
    }

    /* Open accounts manager. */
    private void on_open_accounts(GLib.SimpleAction action,
                                  GLib.Variant? parameter) {
        Utils.DBus.launch_online_accounts.begin();
    }

    /* Save cache to disk. */
    private void on_shutdown() {
        Settings.Cache.get_default().save();
    }
}