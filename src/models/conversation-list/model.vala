/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Model for conversations list. */
public class Mail.Models.ConversationList.Model : Engine.Conversations.Model {

    private GLib.Queue<unowned Engine.Conversations.Conversation> queue =
        new GLib.Queue<unowned Engine.Conversations.Conversation>();

    private uint queue_source_id = 0;

     /**
     * Add conversation to model queue.
     *
     * @param conversation - {@link Engine.Conversations.Conversation}
     */
    public override void add_to_queue(
                               Engine.Conversations.Conversation conversation) {
        lock (this.queue) {
            this.queue.push_tail(conversation);
        }
    }

    /**
     * Remove conversation from model.
     *
     * @param conversation - {@link Engine.Conversations.Conversation}
     */
    public override void remove(Engine.Conversations.Conversation conversation) {
        lock (this.queue) {
            bool removed = this.queue.remove(conversation);
            if (!removed) {
                this.remove_conversation(conversation);
            }
        }
    }

    /** Proceed the queue */
    public override void proceed_queue() {
        if (this.queue_source_id != 0) {
            return;
        }

        this.queue_source_id = GLib.Idle.add(() => {
            // We do not return from lock, Vala really does not like it
            bool ret = Source.CONTINUE;

            lock (this.queue) {
                if (this.queue.length == 0) {
                    this.queue_source_id = 0;
                    ret = Source.REMOVE;
                } else {
                    var conversation = this.queue.pop_head();
                    this.add_conversation(conversation);
                }
            }
            return ret;
        });
    }

    /** Cancel queue processing. */
    public override async void cancel_queue() {
        lock (this.queue) {
            if (this.queue_source_id != 0) {
                GLib.Source.remove(this.queue_source_id);
                this.queue_source_id = 0;
            }
        }

        new GLib.Thread<void>(null, () => {
            this.queue.clear();
            this.reset_conversations();
            GLib.Idle.add(cancel_queue.callback, GLib.Priority.LOW);
        });
        yield;
    }
}