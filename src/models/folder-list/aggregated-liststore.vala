/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Represent a liststore aggregating others liststores. */
public class Mail.Models.FolderList.AggregatedListStore : ListStore {

    private GLib.GenericArray<ListStore> aggregated_stores =
        new GLib.GenericArray<ListStore>();

    private Gee.HashMap<string, AggregatedFolder> aggregated_folders =
        new Gee.HashMap<string, AggregatedFolder>();

    /** Create a new aggregated liststore. */
    public AggregatedListStore() {
        base();
    }

    /**
     * Add liststore to aggregation.
     *
     * @param liststore - {@link ListStore}
     */
    public void add_liststore(ListStore liststore) {
        if (liststore in this.aggregated_stores.data) {
            return;
        }

        this.aggregated_stores.add(liststore);

        liststore.folder_added.connect(on_folder_added);
        liststore.folder_removed.connect(on_folder_removed);

        foreach (var folder in liststore) {
            add_folder(folder);
        }
    }

    /**
     * Remove liststore from aggregation.
     *
     * @param liststore - {@link ListStore}
     */
    public void remove_liststore(ListStore liststore) {
        liststore.folder_added.disconnect(on_folder_added);
        liststore.folder_removed.disconnect(on_folder_removed);
        this.aggregated_stores.remove(liststore);
        foreach (var folder in liststore) {
            remove_folder(folder);
        }
    }

    /* Add folder to liststore */
    private void add_folder(Engine.Folders.Folder folder) {
        var aggregated_folder = this.aggregated_folders.get(folder.path);
        if (aggregated_folder == null) {
            aggregated_folder = new AggregatedFolder();
            this.aggregated_folders.set(folder.path, aggregated_folder);
            // We need to add an initial folder before adding aggregated
            // folder to the current liststore
            aggregated_folder.add((Folder) folder);
            this.add(aggregated_folder);
        } else {
            bool had_children = aggregated_folder.has_children;
            aggregated_folder.add((Folder) folder);
            if (aggregated_folder.has_children != had_children) {
                uint? position = this.get_position(aggregated_folder);
                if (position != null) {
                    items_changed(position, 1, 1);
                }
            }
        }
    }

    /* Remove folder from liststore */
    private void remove_folder(Engine.Folders.Folder folder) {
        var aggregated_folder = this.aggregated_folders.get(folder.path);
        if (aggregated_folder != null) {
            if (aggregated_folder.size == 1) {
                this.remove(aggregated_folder);
                this.aggregated_folders.unset(folder.path);
            }
            aggregated_folder.remove((Folder) folder);
        }
    }

    /*
     * When a folder is added to aggregated liststores, add it
     * to liststore
     */
    private void on_folder_added(Engine.Folders.Folder folder) {
        add_folder(folder);
    }

    /*
     * When a folder is removed from aggregated liststores, remove it
     * from liststore
     */
    private void on_folder_removed(Engine.Folders.Folder folder) {
        remove_folder(folder);
    }
}