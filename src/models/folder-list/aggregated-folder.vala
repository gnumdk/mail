/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/**
 * Represent a folder aggregating others folders
 */
public class Mail.Models.FolderList.AggregatedFolder : Engine.Folders.Folder {

    /** Folder aggregation size. */
    public uint size { get { return this.folders.size; } }

    /** True if any aggregated folder has children. */
    public override bool has_children {
        get {
            return Utils.traverse<Folder>(this.folders).any(
                folder => folder.has_children
            );
        }
        set {}
    }

    /** Aggregated folder path. */
    public override string path {
        get {
            return this.folders.size > 0 ? this.folders.get(0).path : "";
        }
        set {}
    }

    /** Aggregated folder name. */
    public override string name {
        get {
            return this.folders.size > 0 ? this.folders.get(0).name : "";
        }
        set {}
    }

    /** Aggregated folder icon name. */
    public override string icon_name {
        get {
            return this.folders.size > 0 ? this.folders.get(0).icon_name : "";
        }
        set {}
    }

    /** Aggregated folder total messages count. */
    public override uint total {
        get {
            return Utils.traverse<Folder>(this.folders).sum("total");
        }
        set {}
    }

    /** Aggregated folder unread messages count. */
    public override uint unread {
        get {
            return Utils.traverse<Folder>(this.folders).sum("unread");
        }
        set {}
    }

    /** Aggregated folder type. */
    public override Engine.Folders.Properties.Type folder_type {
        get {
            return this.folders.size > 0 ?
                this.folders.get(0).folder_type :
                Engine.Folders.Properties.Type.OTHERS;
        }
        set {}
    }

    private Gee.ArrayList<Folder> folders =
        new Gee.ArrayList<Folder>();

    /** Create a new aggregated folder. */
    public AggregatedFolder() {
        base();
    }

    /**
     * Add folder to aggregation.
     *
     * @param folder - {@link Folder}
     */
    public void add(Folder folder) {
        this.folders.add(folder);
        if (this.folders.size == 1) {
            notify_property("path");
            notify_property("name");
            notify_property("icon_name");
            notify_property("folder_type");
        }
        notify_property("total");
        notify_property("unread");
        inner_folder_added(folder);
    }

    /**
     * Remove folder from aggregation.
     *
     * @param folder - {@link Folder}
     */
    public void remove(Folder folder) {
        this.folders.remove(folder);
        notify_property("total");
        notify_property("unread");
        inner_folder_removed(folder);
    }

    /** Get inner folders. */
    public override Gee.ArrayList<Engine.Folders.Folder> get_inner_folders() {
        return this.folders;
    }
}