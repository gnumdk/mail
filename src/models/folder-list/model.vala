/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Model showing folders as a tree. */
public class Mail.Models.FolderList.Model : Engine.Folders.Model<ListStore> {

    private const uint SYNC_INTERVAL = 15 * 60;

    private Engine.Accounts.Account account;
    private bool syncing = false;

    private Utils.Cancellable cancellable = new Utils.Cancellable();

    /**
     * Create a new model for account.
     *
     * @param account - {@link Engine.Accounts.Account}
     */
    public Model(Engine.Accounts.Account account) {
        base(new ListStore());
        this.account = account;
        this.account.online_changed.connect(on_online_changed);

        // For a strange reason, folder_created, folder_deleted signals do not
        // work. BTW, Thunderbird/Evolution/Geary are unable to detect folder
        // changes without pooling
        GLib.Timeout.add_seconds(SYNC_INTERVAL, () => {
            if (!this.syncing && this.account.online) {
                sync.begin();
            }
            return Source.CONTINUE;
        });
    }

    /**
     * Sync the liststore with account's Camel store at path.
     *
     * @param liststore - {@link ListStore} to sync. If null root store
     * is used
     * @param path - IMAP path to sync
     */
    public async void sync(ListStore? liststore=null, string? path=null) {
        this.syncing = true;

        if (liststore == null) {
            liststore = this.root_store;
        }

        // Ask liststore to watch for orphan folders
        liststore.watch_missing_folders();

        // Populate liststore and clean up existing folders
        yield populate_store_for_path(liststore, path);

        // Ask liststore to remove orphan folders
        liststore.clean_missing_folders();

        this.syncing = false;
    }

    /**
     * Get a model for leaf path.
     *
     * @param path - Leaf IMAP path
     *
     * @return a new model
     */
    public GLib.ListModel? get_leaf_for_path(string path) {
        var liststore = this.leaf_stores.get(path);
        if (liststore == null) {
            liststore = new ListStore();
            this.leaf_stores.set(path, liststore);
        }
        sync.begin(liststore, path);
        return liststore;
    }

    /** Cancel model loading. */
    public void cancel() {
        this.cancellable.cancel();
    }

    /**
     * Get a new model for leaf.
     *
     * @param object - {@link Engine.Folders.Folder}
     *
     * @return a new model
     */
    protected override GLib.ListModel? get_leaf_store(GLib.Object object) {
        var folder = (Engine.Folders.Folder) object;

        if (!folder.has_children) {
            return null;
        }

        return (GLib.ListModel?) get_leaf_for_path(folder.path);
    }

    /* Populate store for path. */
    private async void populate_store_for_path(ListStore liststore,
                                               string? path) {
        var info = yield Engine.Folders.Getter.info(
            this.account.store,
            path,
            Camel.StoreGetFolderInfoFlags.NO_VIRTUAL |
            Camel.StoreGetFolderInfoFlags.REFRESH |
            Camel.StoreGetFolderInfoFlags.SUBSCRIBED,
            this.cancellable.cancellable
        );
        yield populate_store(liststore, info);
    }

    /* Populate store with info. */
    private async void populate_store(
                                ListStore liststore,
                                Engine.Folders.Getter info) {
        foreach (var folder_info in info) {
            yield this.add_folder(liststore, folder_info);
        }
    }

    /*
     * Add folder to liststore and register it with current account.
     *
     * If folder already exists, it is updated.
     */
    private async void add_folder(ListStore liststore,
                                  Camel.FolderInfo folder_info) {
        Engine.Folders.Folder? folder = yield Utils.Folders.get_folder(
            this.account,
            folder_info,
            this.cancellable.cancellable
        );

        // Ignore invalid folder but do not ignore children
        if (folder == null) {
            yield populate_store_for_path(liststore, folder_info.full_name);
            return;
        }

        if (!this.account.has_folder(folder.path))
            this.account.register_folder(folder);

        yield add_or_update_folder(liststore, folder);
    }

    /**
     * Add folder to liststore.
     *
     * If folder already exists, it is updated.
     */
    private async void add_or_update_folder(ListStore liststore,
                                            Engine.Folders.Folder folder) {
        uint? position = Utils.ListStore.get_position(
            liststore, folder.path
        );

        // Tell liststore this folder is not missing (see sync())
        liststore.keep_folder(folder);

        if (position != null) {
            yield update_folder(liststore, folder, position);
        } else {
            liststore.add(folder, true);
        }
    }

    /* Update folder in liststore at position. */
    private async void update_folder(ListStore liststore,
                                     Engine.Folders.Folder folder,
                                     uint position) {

        var children_store = this.leaf_stores.get(folder.path);
        var existing = (Engine.Folders.Folder) liststore.get_item(
            position
        );

        if (existing.has_children != folder.has_children) {
            existing.has_children = folder.has_children;
            liststore.items_changed(position, 1, 1);
        }

        if (children_store != null) {
            if (folder.has_children) {
                yield sync(children_store, folder.path);
            } else {
                remove_leaf(children_store, existing);
            }
        }
    }

    /* Remove leaf from liststore. */
    private void remove_leaf(ListStore liststore,
                             Engine.Folders.Folder folder) {
        for (var i=0; i < liststore.get_n_items(); i++) {
            var leaf = (Engine.Folders.Folder) liststore.get_item(i);
            this.account.unregister_folder(leaf);
            if (this.leaf_stores.has_key(leaf.path)) {
                remove_leaf(this.leaf_stores.get(leaf.path), leaf);
            }
        }
        this.leaf_stores.unset(folder.path);
    }

    /* Sync folders on network status change. */
    private void on_online_changed(bool online) {
        if (!this.syncing && online) {
            sync.begin();
        }
    }
}