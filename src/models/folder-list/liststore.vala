/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Advanceed liststore. */
public class Mail.Models.FolderList.ListStore : GLib.ListModel, GLib.Object {

    /**
     * Emit the folder_added signal.
     *
     * A folder has been added to liststore
     *
     * @param folder - {@link Engine.Folders.Folder}
     */
    public signal void folder_added(Engine.Folders.Folder folder);

    /**
     * Emit the folder_removed signal.
     *
     * A folder has been removed from liststore
     *
     * @param folder - {@link Engine.Folders.Folder}
     */
    public signal void folder_removed(Engine.Folders.Folder folder);

    private GLib.Sequence<Engine.Folders.Folder> folders =
        new GLib.Sequence<Engine.Folders.Folder>();

    private Gee.HashMap<string, Engine.Folders.Folder> recorded =
        new Gee.HashMap<string, Engine.Folders.Folder>();

    /** Create a new liststore. */
    public ListStore() {
        Object();
    }

    /**
     * Get the item at position.
     *
     * @param position - Position of the item to fetch
     *
     * @return an object
     */
    public GLib.Object? get_item(uint position) {
        return this.folders.get_iter_at_pos((int) position).get();
    }

    /**
     * Gets the type of the items in liststore.
     *
     * @return GLib.Type
     */
    public GLib.Type get_item_type() {
        return typeof(Engine.Folders.Folder);
    }

    /**
     * Gets the number of items in liststore.
     */
    public uint get_n_items() {
        return this.folders.get_length();
    }

    /**
     * Add folder to liststore.
     *
     * @param folder - {@link Engine.Folders.Folder} to add
     * @param notify - if True, folder_added() is emitted
     */
    public void add(Engine.Folders.Folder folder, bool notify=false) {
        var iter = this.folders.insert_sorted(
            folder,
            (a, b) => {
                var item_a = (Engine.Folders.Folder) a;
                var item_b = (Engine.Folders.Folder) b;

                if (item_a.folder_type >= Engine.Folders.Properties.Type.TAG &&
                    item_b.folder_type >= Engine.Folders.Properties.Type.TAG) {
                    return GLib.strcmp(item_a.name.down(), item_b.name.down());
                }
                return item_a.folder_type - item_b.folder_type;
            }
        );

        if (notify) {
            folder_added(folder);
        }

        items_changed(iter.get_position(), 0, 1);
    }

    /**
     * Remove folder from liststore.
     *
     * @param folder - {@link Engine.Folders.Folder} to add
     * @param notify - if True, folder_removed() is emitted
     */
    public void remove(Engine.Folders.Folder folder, bool notify=false) {
        var iter = this.folders.get_begin_iter();
        while (iter != null) {
            if (iter.get() == folder) {
                break;
            }
            iter = iter.next();
        }

        return_if_fail(iter != null);

        uint position = iter.get_position();
        iter.remove();

        if (notify) {
            folder_removed(folder);
        }

        items_changed(position, 1, 0);
    }

    /**
     * Get folder position in liststore.
     *
     * @param folder - {@link Engine.Folders.Folder}
     *
     * @return position or null if not found
     */
    public uint? get_position(Engine.Folders.Folder folder) {
        var iter = this.folders.get_begin_iter();
        while (iter != null) {
            if (iter.get() == folder) {
                return iter.get_position();
            }
            iter = iter.next();
        }
        return null;
    }

    /** Start watching for missing folder. */
    public void watch_missing_folders() {
        this.recorded = new Gee.HashMap<string, Engine.Folders.Folder>();
        foreach (var folder in this.folders) {
            this.recorded.set(folder.path, folder);
        }
    }

    /**
     * Keep folder in liststore.
     *
     * @param folder - {@link Engine.Folders.Folder}
     */
    public void keep_folder(Engine.Folders.Folder folder) {
        this.recorded.unset(folder.path);
    }

    /** Remove missing folders. */
    public void clean_missing_folders() {
        foreach (var folder in this.recorded.values) {
            remove(folder, true);
        }
    }

    /**
     * Get a new iterator.
     *
     * @return Iterator
     */
    public Iterator iterator() {
        return new Iterator(this.folders);
    }

    public class Iterator {
        private GLib.SequenceIter<Engine.Folders.Folder> iter;

        public Iterator(GLib.Sequence<Engine.Folders.Folder> items) {
            this.iter = items.get_begin_iter();
        }

        public bool next() {
            if (!this.iter.is_end()) {
                this.iter = this.iter.next();
            }
            return has_next();
        }

        public bool has_next() {
            return !this.iter.is_end();
        }

        public Engine.Folders.Folder get() {
            return this.iter.get();
        }
    }
}