/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** An IMAP folder. */
public class Mail.Models.FolderList.Folder : Engine.Folders.Folder {

    private Engine.Folders.Properties properties;

    /*
     * Create a new folder.
     *
     * @param info - {@link Camel.FolderInfo}
     * @param account - {@link Engine.Accounts.Account account}
     * @param cancellable - {@link GLib.Cancellable}
     */
    public async Folder(Camel.FolderInfo info, Engine.Accounts.Account account,
                        GLib.Cancellable cancellable) throws GLib.Error {
        base();
        this.account = account;
        this.properties = new Engine.Folders.Properties(account);
        this.folder_type = yield this.properties.get_folder_type(info);
        this.icon_name = Engine.Folders.Properties.get_icon_name(
            this.folder_type
        );
        this.has_children = info.child != null;
        this.camel_folder = yield account.store.get_folder(
            info.full_name,
            Camel.StoreGetFolderFlags.NONE,
            GLib.Priority.DEFAULT,
            cancellable
        );

        this.camel_folder.bind_property(
            "display-name", this, "name", GLib.BindingFlags.SYNC_CREATE,
            translate_name
        );
        this.camel_folder.bind_property(
            "full-name", this, "path", GLib.BindingFlags.SYNC_CREATE
        );
        this.camel_folder.get_folder_summary().bind_property(
            "unread-count", this, "unread", GLib.BindingFlags.SYNC_CREATE
        );
         this.camel_folder.get_folder_summary().bind_property(
            "visible-count", this, "total", GLib.BindingFlags.SYNC_CREATE
        );
    }

    /**
     * Get inner folders.
     *
     * @return List of inner folders
     *
     */
    public override Gee.ArrayList<Engine.Folders.Folder> get_inner_folders() {
        var folders = new Gee.ArrayList<Engine.Folders.Folder>();
        folders.add(this);
        return folders;
    }

    /* Translate folder name. */
    private bool translate_name(Binding binding, Value from_value,
                                ref Value to_value) {
        string? translated = this.properties.get_folder_translated_name(
            this.folder_type
        );

        if (translated == null)
            to_value.set_string(from_value.get_string());
        else
            to_value.set_string(translated);

        return true;
    }
}