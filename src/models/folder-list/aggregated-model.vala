/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Model showing all accounts aggregated into one. */
public class Mail.Models.FolderList.AggregatedModel :
                                     Engine.Folders.Model<AggregatedListStore> {

    private GLib.GenericArray<Model> models = new GLib.GenericArray<Model>();

    /** Create a new aggregated model. */
    public AggregatedModel() {
        base(new AggregatedListStore());
    }

    /**
     * Add model to aggregation.
     *
     * @param model - @{link model}
     */
    public void add_model(Model model) {
        this.models.add(model);
        this.root_store.add_liststore(model.root_store);
    }

    /**
     * Remove model from aggregation.
     *
     * @param model - @{link model}
     */
    public void remove_model(Model model) {
        this.models.remove(model);
        this.root_store.remove_liststore(model.root_store);
    }

    /**
     * Get a new model for leaf.
     *
     * @param object - @{Engine.Folders.Folder}
     */
    protected override GLib.ListModel? get_leaf_store(GLib.Object object) {
        var folder = (Engine.Folders.Folder) object;

        if (!folder.has_children) {
            return null;
        }

        AggregatedListStore agg_liststore = this.leaf_stores.get(folder.path);
        if (agg_liststore == null) {
            agg_liststore = new AggregatedListStore();
            this.leaf_stores.set(folder.path, agg_liststore);
        }

        foreach (var model in this.models) {
            var liststore = (ListStore) model.get_leaf_for_path(folder.path);
            if (liststore != null) {
                agg_liststore.add_liststore(liststore);
            }
        }

        return agg_liststore;
    }
}