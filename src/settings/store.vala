/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Allows saving settings to gsettings. */
public class Mail.Settings.Store : GLib.Settings {

    private static Store _store;
    public static unowned Store get_default () {
        if (_store == null) {
            _store = new Store();
        }
        return _store;
    }

    /**  Get a new settings store. */
    public Store() {
        Object(schema_id: "org.adishatz.Mail");
    }

    /** Startup account id. */
    public string startup_account_id {
        owned get {
            return this.get_string("startup-account-id");
        }
        set {
            this.set_string("startup-account-id", value);
        }
    }
}