/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Allows caching data to a keyfile. */
public class Mail.Settings.Cache {

    private static string FILEPATH = "%s/%s".printf(
        GLib.Environment.get_user_cache_dir(),
        Application.APP_ID
    );
    private static string EXPANDED = "folder_expanded";

    private static Cache _cache;
    public static unowned Cache get_default () {
        if (_cache == null) {
            _cache = new Cache();
        }
        return _cache;
    }

    private GLib.KeyFile key_file = new GLib.KeyFile();

    /** Create a new settings cache. */
    public Cache() {
        try {
            File file = File.new_for_path(FILEPATH);
            if (!file.query_exists()) {
                file.make_directory_with_parents(null);
            }
            this.key_file.load_from_file(
                "%s/keyfile".printf(FILEPATH),
                GLib.KeyFileFlags.NONE
            );
        } catch (GLib.Error error) {
            warning("Can't open cache keyfile: %s\n", error.message);
        }
    }

    /** Save cache to disk. */
    public void save() {
        try {
            this.key_file.save_to_file("%s/keyfile".printf(FILEPATH));
        } catch (GLib.Error error) {
            warning("Can't save cache keyfile: %s\n", error.message);
        }
    }

    /**
     * Get folder expanded state.
     *
     * @param folder - {@link Engine.Folders.Folder}
     */
    public bool get_folder_expanded(Engine.Folders.Folder folder) {
        try {
            return this.key_file.get_boolean(EXPANDED, get_folder_key(folder));
        } catch {
            return false;
        }
    }

    /**
     * Set folder expanded state.
     *
     * @param folder - {@link Engine.Folders.Folder}
     * @param expanded - True if folder is expanded
     */
    public void set_folder_expanded(Engine.Folders.Folder folder,
                                    bool expanded) {
        var key = get_folder_key(folder);
        try {
            if (expanded) {
                this.key_file.set_boolean(EXPANDED, key, true);
            } else if (this.key_file.has_group(EXPANDED) &&
                       this.key_file.has_key(EXPANDED, key)) {
                this.key_file.remove_key(EXPANDED, key);
            }
        } catch (GLib.Error error) {
            warning(error.message);
        }
    }

    /* Get folder key name. */
    private string get_folder_key(Engine.Folders.Folder folder) {
        var path = new StringBuilder();

        if (folder.account == null) {
            path.append("aggregated_");
            path.append(folder.path);
        } else {
            path.append(folder.account.id);
            path.append("_");
            path.append(folder.path);
        }
        return path.str;
    }
}