/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** Item representing a folder in folder list. */
[GtkTemplate (ui = "/org/adishatz/Mail/folder-list-item.ui")]
public class Mail.FolderList.Item : Adw.Bin {

    /** Associated {@link Gtk.TreeListRow}. */
    private Gtk.TreeListRow? _row = null;
    public Gtk.TreeListRow? row {
        get {
            return this._row;
        }
        set {
            if (value != null && value != this._row) {
                this._row = value;
                update((Engine.Folders.Folder) this._row.item);
                GLib.Timeout.add(150, wait_for_row_hack);
            }
        }
    }

    [GtkChild] private unowned Gtk.Label name_label;
    [GtkChild] private unowned Gtk.Label unread_label;
    [GtkChild] private unowned Gtk.Image icon;

    /* Update folder item. */
    private void update(Engine.Folders.Folder folder) {
        this.name_label.label = folder.name;
        this.icon.icon_name = folder.icon_name;
        if (folder.unread > 0) {
            this.unread_label.label = folder.unread.to_string();
            this.unread_label.show();
        } else {
            this.unread_label.hide();
        }
    }

    /*
     * When added to Gtk.ListView sets expanded to false and
     * refused any update for some times :(
     * Am I doing something wrong ?
     */
    private bool wait_for_row_hack() {
        var folder = (Engine.Folders.Folder) this._row.item;
        this._row.expanded = folder.expanded;
        this._row.bind_property(
            "expanded",
            this._row.item,
            "expanded",
            GLib.BindingFlags.BIDIRECTIONAL
        );
        return Source.REMOVE;
    }
}