/*
 * Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
 */

/** ListView showing a folder tree. */
[GtkTemplate (ui = "/org/adishatz/Mail/folder-list-view.ui")]
public class Mail.FolderList.View : Adw.Bin {

    public string title { get; private set; default = " "; }
    public Engine.Folders.Folder selected { get; private set; default=null; }

    [GtkChild] private unowned Gtk.SingleSelection model;

    /**
     * Set view model.
     *
     * @param model - {@link Engine.Folders.Model}
     */
    public void set_model(Engine.Folders.Model model) {
        this.model.set_model(null);
        this.model.set_model(model.root_model);
    }

    /* When a folder is selected, update title and load conversations. */
    [GtkCallback]
    private void on_folder_selected_notify() {
        var row = (Gtk.TreeListRow) this.model.selected_item;
        if (row != null) {
            this.selected = (Engine.Folders.Folder) row.item;
            this.title = this.selected.name;
            Controller.get_default().load_conversations.begin(this.selected);
        }
    }
}