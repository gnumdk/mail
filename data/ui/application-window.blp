using Gtk 4.0;
using Adw 1;

template $MailWindow : Adw.ApplicationWindow {
  width-request: 360;
  height-request: 200;
  default-width: 1200;
  default-height: 800;
  title: _("Mail");
  content: Adw.OverlaySplitView overlay_split_view {
    min-sidebar-width: 260;
    max-sidebar-width: 400;
    sidebar-width-fraction: 0.2;
    sidebar: Adw.NavigationPage {
      title: _("Mail");
      child: Adw.ToolbarView {
        [top]
        Adw.HeaderBar {
          [start]
          $MailHeaderBarAccountsButton accounts_button {
            active_account_changed => $on_active_account_changed();
          }
          [start]
          Gtk.ToggleButton navigation_split_view_btn1 {
                active: bind-property overlay_split_view.show-sidebar bidirectional;
                icon-name: "sidebar-show-symbolic";
                visible: false;
              }
          [end]
          Gtk.MenuButton {
            primary: true;
            icon-name: "open-menu-symbolic";
            menu-model: primary_menu;
          }
        }
        content: $MailFolderListView folder_list_view {
        };
      };
    };
    content: Adw.NavigationPage navigation_page {
      title: bind-property folder_list_view.title;
      child: Adw.NavigationSplitView navigation_split_view {
        max-sidebar-width: 500;
        sidebar-width-fraction: 0.4;
        sidebar: Adw.NavigationPage inbox_page {
          title: bind-property folder_list_view.title;
          tag: "inbox";
          child: Adw.ToolbarView {
            [top]
            Adw.HeaderBar {
              [start]
              Gtk.ToggleButton navigation_split_view_btn2 {
                active: bind-property overlay_split_view.show-sidebar bidirectional;
                icon-name: "sidebar-show-symbolic";
                visible: false;
              }
              [start]
              Gtk.ToggleButton start_inbox_search_btn {
                active: bind-property inbox_search_bar.search-mode-enabled bidirectional;
                icon-name: "edit-find-symbolic";
              }
              [end]
              Gtk.ToggleButton end_inbox_search_btn {
                active: bind-property inbox_search_bar.search-mode-enabled bidirectional;
                icon-name: "edit-find-symbolic";
                visible: false;
              }
              [end]
              Gtk.Button {
                icon-name: "selection-mode-symbolic";
              }
            }
            [top]
            Gtk.SearchBar inbox_search_bar {
              key-capture-widget: inbox_page;
              child: Adw.Clamp {
                maximum-size: 400;
                child: Gtk.SearchEntry inbox_search_entry {
                  hexpand: true;
                };
              };
            }
            content: Gtk.Overlay {
              [overlay]
              Gtk.Button {
                halign: end;
                valign: end;
                icon-name: "mail-message-new-symbolic";
                styles ["suggested-action", "circular", "fab"]
              }
              child: $MailConversationListView conversation_list_view {
              };
            };
          };
        };
        content: Adw.NavigationPage message_page {
          tag: "message";
          title: _("Message");
          child: Adw.ToolbarView {
            [top]
            Adw.HeaderBar {
              show-title: false;

              [start]
              Gtk.ToggleButton navigation_split_view_btn3 {
                active: bind-property overlay_split_view.show-sidebar bidirectional;
                icon-name: "sidebar-show-symbolic";
                visible: false;
              }
              [start]
              Gtk.Button reply_btn {
                icon-name: 'mail-reply-sender-symbolic';
              }
              [start]
              Gtk.Button reply_all_btn {
                icon-name: 'mail-reply-all-symbolic';
              }
              [start]
              Gtk.Button forward_btn {
                icon-name: 'mail-forward-symbolic';
              }

              [end]
              Gtk.Button trash_btn {
                icon-name: 'user-trash-symbolic';
              }
            }
            [top]
            Gtk.SearchBar message_search_bar {
              key-capture-widget: message_page;
              child: Adw.Clamp {
                maximum-size: 400;
                child: Gtk.SearchEntry message_search_entry {
                  hexpand: true;
                };
              };
            }
            content: $MailConversationsView conversations_view {
            };
            [bottom]
            Gtk.Box toolbar {
              visible: false;
              homogeneous: true;
              Gtk.Button {
                icon-name: 'mail-reply-sender-symbolic';
              }
              Gtk.Button {
                icon-name: 'mail-reply-all-symbolic';
              }
              Gtk.Button {
                icon-name: 'mail-forward-symbolic';
              }
              Gtk.Button {
                icon-name: 'user-trash-symbolic';
              }
              styles ["toolbar"]
            }
          };
        };
      };
    };
  };
  Adw.Breakpoint {
    condition ("max-width: 860sp")
    setters {
      overlay_split_view.collapsed: true;
      navigation_split_view_btn1.visible: true;
      navigation_split_view_btn2.visible: true;
    }
  }
  Adw.Breakpoint {
    condition ("max-width: 500sp")
    setters {
      navigation_split_view.collapsed: true;
      navigation_split_view.show-content: true;
      overlay_split_view.collapsed: true;
      overlay_split_view.show-sidebar: false;

      navigation_split_view_btn1.visible: true;
      navigation_split_view_btn2.visible: true;
      navigation_split_view_btn3.visible: true;

      start_inbox_search_btn.visible: false;
      end_inbox_search_btn.visible: true;

      reply_btn.visible: false;
      reply_all_btn.visible: false;
      forward_btn.visible: false;
      trash_btn.visible: false;
      toolbar.visible: true;

    }
  }
}
menu primary_menu {
  section {
    item {
      label: _("A_ccounts");
      action: "app.accounts";
    }
  }
  section {
    item {
      label: _("_Preferences");
      action: "app.preferences";
    }
    item {
      label: _("_Keyboard Shortcuts");
      action: "win.show-help-overlay";
    }
    item {
      label: _("_Help");
      action: "app.help";
    }
    item {
      label: _("_About Mail");
      action: "app.about";
    }
  }
}