#!/bin/bash

function generate_po()
{
    find src -name *.vala > po/POTFILES
    find data -name *.ui >> po/POTFILES
    find data -name *.in >> po/POTFILES
    find data -name *.blp >> po/POTFILES
    xgettext --keyword=_ --keyword=C_:1c,2 --add-comments=Translators --from-code=UTF-8 $(cat po/POTFILES) -o po/mail.pot
    sed -i 's/CHARSET/UTF-8/g' po/mail.pot
    while read lang
    do
        [ ! -e po/$lang.po ] && touch po/$lang.po
        msgmerge -N po/$lang.po po/mail.pot > /tmp/$$$lang.po
        mv /tmp/$$$lang.po po/$lang.po
    done < po/LINGUAS
}

[[ -e po ]] && generate_po
